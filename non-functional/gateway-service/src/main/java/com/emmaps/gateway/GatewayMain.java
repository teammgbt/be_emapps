package com.emmaps.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */

@SpringBootApplication
@EnableDiscoveryClient
@RestController
public class GatewayMain {
	public static void main(String[] args) {
		SpringApplication.run(GatewayMain.class, args);
	}

	@GetMapping("")
	public String checkIfWorks() {
		return "Gateway server is up";
	}
}
