# README #

This is BE service for Event Management application. The apps using microservice 
for its architecture. Following is the services included in this project :
- Non-Functional 
    * Configuration service
    * Registry Service
    * Gateway Service
    
- Functional 
    * Authorization service
    * Event service
    * TBA
 
### How do I get set up? ###

* Summary of set up
This project using Maven as building system configuration. You can use
IDE like inteliJ to open it.
 
* Configuration
* Dependencies
* Database configuration
* How to run tests
    * run this command ``mvn test``
* How to build project
    * run this command ``mvn clean install package``
* Deployment instructions

### How do I run the application locally? ###
There are two ways on how to run this project locally :
- Run each main service manually in order. The order is important to make sure the project can run well.
  We need to start from the non-functional service first, in the following order :
  * Configuration Service
  * Registry Service
  * Gateway Service
  
  Make sure each service has completely started before start the other service.
  And after all the non-functional service has been started completely, run all the functional service.
- Run the project locally using docker (Need to install docker first). Build the project using command ``mvn clean install package``
  to generate the jar file that will be run by docker. On the root project, run this command on terminal to start the project using docker :
  * ``docker-compose up -d``
  
  Try to use bash shell (eg. git bash) to run this command if there is an issue coming up when run the project using docker.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Spring boot](https://spring.io/projects/spring-boot) - Application framework 
* [MongoDB](https://www.mongodb.com/) - Application Database

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

## Authors

* **Dendy Prtha** - *Initial work* - [dendy-senapartha](https://github.com/dendy-senapartha)

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc