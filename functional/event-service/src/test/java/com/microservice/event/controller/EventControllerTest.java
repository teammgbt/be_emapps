package com.microservice.event.controller;

import com.microservice.common.model.ErrorMessage;
import com.microservice.common.model.ResponseWrapper;
import com.microservice.event.common.CommonTest;
import com.microservice.event.model.Event;
import com.microservice.event.security.filter.AWSCognitoIdTokenProcessor;
import com.microservice.event.service.EventServiceImpl;
import com.microservice.event.validation.EventValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Ryan_W737 on June 29, 2020
 */
@RunWith(MockitoJUnitRunner.class)
public class EventControllerTest extends CommonTest {
    @InjectMocks
    private EventController eventController;

    @Mock
    private EventServiceImpl service;

    @Mock
    private HttpServletRequest request;

    @Mock
    private AWSCognitoIdTokenProcessor awsCognitoIdTokenProcessor;

    @Mock
    private EventValidator validation;

    @Test
    public void testCreateEventReturnSuccess() throws Exception {
        Event event = createEvent();
        when(service.addEvent(event)).thenReturn(event);
        ResponseEntity responseEntity = eventController.createEvent(request, event);
        assertEquals(200, responseEntity.getStatusCode().value());
    }

    @Test
    public void testCreateEventReturnFailed() throws Exception {
        Event event = createEvent();
        event.setName(null);

        List<ErrorMessage> errorMessages = new ArrayList<>();
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setCode(HttpStatus.NOT_ACCEPTABLE.toString());
        errorMessage.setMessage("Name");
        errorMessages.add(errorMessage);

        when(validation.validate(event)).thenReturn(errorMessages);
        ResponseEntity responseEntity = eventController.createEvent(request, event);
        assertEquals(406, responseEntity.getStatusCode().value());
    }

    @Test
    public void testGetMyCreatedEventsReturnSuccess() throws Exception {

        String token = "Esadasd12321312-Token";
        String username = "test@mail.com";

        List<Event> events = createListEvent();

        //mock
        doReturn(events).when(service).getAllByCreatedUserEmail(anyString(), anyInt(), anyInt());

        when(request.getHeader("Authorization"))
                .thenReturn(token);

        when(awsCognitoIdTokenProcessor.getUserName(token))
                .thenReturn(username);

        //test
        ResponseEntity<ResponseWrapper> responseEntity = eventController.getMyCreatedEvents(request, 0, 2);

        //Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        responseWrapper.setErrors(new ArrayList<>());
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());

        List<Event> resultResponse = (List<Event>) responseWrapper.getData();
        assertEquals(events, resultResponse);
    }

    @Test
    public void testSetAsCancelledReturnSuccess() throws Exception {
        Event event = createEvent();
        when(validation.validateEventSetAsCancelled(event.getId())).thenReturn(new ArrayList<>());
        ResponseEntity responseEntity = eventController.setAsCancelled(request, event.getId());
        assertEquals(200, responseEntity.getStatusCode().value());
    }

    @Test
    public void testSetAsCancelledReturnFailed() throws Exception {
        Event event = createEvent();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setMessage("Error Field");
        errorMessage.setCode("Field");
        errorMessages.add(errorMessage);
        when(validation.validateEventSetAsCancelled(event.getId())).thenReturn(errorMessages);
        ResponseEntity responseEntity = eventController.setAsCancelled(request, event.getId());
        assertEquals(406, responseEntity.getStatusCode().value());
    }

    @Test
    public void testUpdateEventReturnSuccess() throws Exception {
        Event event = createEvent();
        when(validation.validateUpdateEvent(event, event.getId())).thenReturn(new ArrayList<>());
        ResponseEntity responseEntity = eventController.updateEvent(request, event.getId(), event);
        assertEquals(200, responseEntity.getStatusCode().value());
    }

    @Test
    public void testUpdateEventReturnFailed() throws Exception {
        Event event = createEvent();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setMessage("Error Field");
        errorMessage.setCode("Field");
        errorMessages.add(errorMessage);
        when(validation.validateUpdateEvent(event, event.getId())).thenReturn(errorMessages);
        ResponseEntity responseEntity = eventController.updateEvent(request, event.getId(), event);
        assertEquals(406, responseEntity.getStatusCode().value());
    }

    @Test
    public void testGetAllEventReturnSuccess() {
        List<Event> events = createListEvent();
        when(service.getAllEvent(1, 10)).thenReturn(events);
        ResponseEntity responseEntity = eventController.getListEvents( "",1, 10);
        verify(service, times(1)).getAllEvent(anyInt(), anyInt());
        assertEquals(200, responseEntity.getStatusCode().value());
    }

    @Test
    public void testGetAllEventWithFilterReturnSuccess() {
        List<Event> events = createListEvent();
        when(service.getAllFilteredEvent("categoryName", 1, 10)).thenReturn(events);
        ResponseEntity responseEntity = eventController.getListEvents("categoryName", 1, 10);
        verify(service, times(1)).getAllFilteredEvent(anyString(), anyInt(), anyInt());
        assertEquals(200, responseEntity.getStatusCode().value());
    }

    @Test
    public void testSearchEventByKeywordReturnSuccess() {
        String keyword = "DUMMY";
        List<Event> events = createListEvent();
        when(service.searchEventByKeyword(keyword, 1, 10)).thenReturn(events);
        ResponseEntity responseEntity = eventController.searchEvent(keyword, 1, 10);
        assertEquals(200, responseEntity.getStatusCode().value());
    }

    @Test
    public void testGetEventDetailReturnSuccess() {
        Event event = createEvent();
        when(service.getById(event.getId())).thenReturn(event);
        ResponseEntity responseEntity = eventController.getEventDetail(event.getId());
        assertEquals(200, responseEntity.getStatusCode().value());
    }
}
