package com.microservice.event.service;

import com.microservice.event.common.CommonTest;
import com.microservice.event.model.EventCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import static org.mockito.Mockito.any;

/**
 * @author Ryan_W737 on June 29, 2020
 */
@RunWith(MockitoJUnitRunner.class)
public class EventCategoryServiceTest extends CommonTest {
    @InjectMocks
    private EventCategoryServiceImpl service;

    @Mock
    private MongoTemplate mongoTemplate;

    @Test
    public void testFindByNameReturnSuccess() {
        EventCategory eventCategory = createEventCategory();
        Mockito.when(mongoTemplate.findOne(any(Query.class), any())).thenReturn(eventCategory);
        EventCategory response = service.findByName(eventCategory.getCategoryName());
        Assert.assertEquals(response.getCategoryName(), eventCategory.getCategoryName());
    }
}
