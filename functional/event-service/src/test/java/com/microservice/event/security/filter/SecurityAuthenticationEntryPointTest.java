package com.microservice.event.security.filter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Ryan_W737 on July 8, 2020
 */
@RunWith(MockitoJUnitRunner.class)
public class SecurityAuthenticationEntryPointTest {
    private static final String USER_NOT_FOUND = "Username not found";

    @InjectMocks
    private SecurityAuthenticationEntryPoint entryPoint;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Test
    public void testCommenceReturnSuccess() throws IOException {
        HttpServletResponse response = new HttpServletResponseWrapper(httpServletResponse);
        PrintWriter writer = new PrintWriter(USER_NOT_FOUND);
        Mockito.when(response.getWriter()).thenReturn(writer);
        AuthenticationException exception = new UsernameNotFoundException(USER_NOT_FOUND);
        entryPoint.commence(httpServletRequest, httpServletResponse, exception);
    }
}
