package com.microservice.event.service;

import com.microservice.event.common.CommonTest;
import com.microservice.event.model.Event;
import com.microservice.event.repository.EventRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * @author Ryan_W737 on June 29, 2020
 */
@RunWith(MockitoJUnitRunner.class)
public class EventServiceTest extends CommonTest {
    private static final int PAGE_NO = 1;
    private static final int PAGE_SIZE = 50;
    private static final String CREATED_DATE = "createdDate";

    @InjectMocks
    private EventServiceImpl service;

    @Mock
    private EventRepository repository;

    @Mock
    MongoTemplate mongoTemplate;

    @Test
    public void testAddEventReturnSuccess() {
        Event event = createEvent();
        when(repository.insert(event)).thenReturn(event);
        Event response = service.addEvent(event);
        assertEquals(response.getId(), event.getId());
    }

    @Test
    public void testGetAllReturnSuccess() {
        List<Event> events = new ArrayList<>();
        Event event = createEvent();
        events.add(event);
        when(repository.findAll()).thenReturn(events);
        List<Event> response = service.getAll();
        assertEquals(response.get(0).getId(), event.getId());
    }

    @Test
    public void testGetByIdReturnSuccess() {
        Event event = createEvent();
        Optional<Event> events = Optional.ofNullable(event);
        when(repository.findById(event.getId())).thenReturn(events);
        Event response = service.getById(event.getId());
        assertEquals(response != null ? response.getId() : null,
                events.isPresent() ? events.get().getId() : null);
    }

    @Test
    public void testDeleteByIdReturnSuccess() {
        Event event = createEvent();
        service.deleteById(event.getId());
        assertNotNull(event.getId());
    }

    @Test
    public void testEditEventReturnSuccess() {
        Event event = createEvent();
        Event updatedEvent = createListEvent().get(0);
        when(repository.findById(event.getId())).thenReturn(Optional.of(event));
        when(repository.save(event)).thenReturn(event);
        Event response = service.editEvent(updatedEvent, "updatedUser", event.getId());
        assertEquals(response.getId(), event.getId());
        assertEquals(response.getName(), updatedEvent.getName());
        assertEquals(response.getTitle(), updatedEvent.getTitle());
        assertEquals(response.getImage(), updatedEvent.getImage());
        assertEquals(response.getVenueAddress(), updatedEvent.getVenueAddress());
    }

    @Test
    public void testGetAllByCreatedUserEmailReturnSuccess() {
        List<Event> events = createListEvent();

        Query query = new Query(Criteria.where("createdBy").is("userName"))
                .with(PageRequest.of(0, 5))
                //default sort by CreatedDate for now
                .with(Sort.by(Sort.Direction.DESC, CREATED_DATE));

        //Mock
        doReturn(events).when(mongoTemplate).find(query, Event.class);

        List<Event> result = service.getAllByCreatedUserEmail("userName", 0, 5);

        assertEquals(events, result);
    }

    @Test
    public void testSetAsCancelledReturnSuccess() {
        Event event = createEvent();
        when(repository.findById(event.getId())).thenReturn(Optional.of(event));
        Event response = service.setAsCancelled(null, event.getId());
        assertEquals(response.getId(), event.getId());
    }

    @Test
    public void testGetAllEventReturnSuccess() {
        List<Event> events = createListEvent();
        Query query = new Query(Criteria.where("statusId").is(1))
                .with(PageRequest.of(PAGE_NO, PAGE_SIZE))
                .with(Sort.by(Sort.Direction.DESC, CREATED_DATE));
        when(mongoTemplate.find(query, Event.class)).thenReturn(events);
        List<Event> result = service.getAllEvent(PAGE_NO, PAGE_SIZE);
        assertEquals(events, result);
    }

    @Test
    public void testSearchByKeywordReturnSuccess() {
        String keyword = "categoryName1";
        List<Event> events = createListEvent();
        when(mongoTemplate.find(any(Query.class), ArgumentMatchers.<Class<Event>>any())).thenReturn(events);
        List<Event> result = service.searchEventByKeyword(keyword, PAGE_NO, PAGE_SIZE);
        assertEquals(events, result);
    }

    @Test
    public void testGetAllEventByFilterReturnSuccess() {
        String categoryName = "categoryName1";
        List<Event> events = createListEvent();
        when(mongoTemplate.find(any(Query.class), ArgumentMatchers.<Class<Event>>any())).thenReturn(events);
        List<Event> result = service.getAllFilteredEvent(categoryName, PAGE_NO, PAGE_SIZE);
        assertEquals(events, result);
    }
}
