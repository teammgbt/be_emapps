package com.microservice.event.validation;

import com.microservice.common.model.ErrorMessage;
import com.microservice.event.common.CommonTest;
import com.microservice.event.model.Event;
import com.microservice.event.model.EventCategory;
import com.microservice.event.service.EventCategoryService;
import com.microservice.event.service.EventService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @author Ryan_W737 on June 29, 2020
 */
@RunWith(MockitoJUnitRunner.class)
public class EventValidatorTest extends CommonTest {
    @InjectMocks
    private EventValidator validator;

    @Mock
    private EventCategoryService service;

    @Mock
    private EventService eventService;

    private static final String DUMMY_GUID = UUID.randomUUID().toString();

    @Test
    public void testValidateReturnSuccess() {
        Event event = createEvent();
        EventCategory eventCategory = createEventCategory();
        when(service.findByName(event.getCategoryName())).thenReturn(eventCategory);
        List<ErrorMessage> response = validator.validate(event);
        assertEquals(0, response.size());
    }

    @Test
    public void testValidateReturnCategoryNameNotFound() {
        Event event = createEvent();
        List<ErrorMessage> response = validator.validate(event);
        assertEquals("Category does not exist", response.get(0).getMessage());
    }

    @Test
    public void testValidateResponseFailed() {
        List<ErrorMessage> response = validator.validate(new Event());
        assertEquals(10, response.size());
    }

    @Test
    public void testValidateEventSetAsCancelledReturnNull() {
        Event event = createEvent();
        when(eventService.getById(event.getId())).thenReturn(event);
        List<ErrorMessage> response = validator.validateEventSetAsCancelled(event.getId());
        assertTrue(response.isEmpty());
    }

    @Test
    public void testValidateEventSetAsCancelledReturnCancelled() {
        Event event = createEvent();
        event.setStatusId(3);
        when(eventService.getById(event.getId())).thenReturn(event);
        List<ErrorMessage> response = validator.validateEventSetAsCancelled(event.getId());
        assertEquals("Event Status already cancelled.", response.get(0).getMessage());
    }

    @Test
    public void testValidateEventSetAsCancelledReturnNotExist() {
        when(eventService.getById(DUMMY_GUID)).thenReturn(null);
        List<ErrorMessage> response = validator.validateEventSetAsCancelled(DUMMY_GUID);
        assertEquals("Event ID is not exist.", response.get(0).getMessage());
    }

    @Test
    public void testValidateUpdateEventReturnNotExist() {
        when(eventService.getById(DUMMY_GUID)).thenReturn(null);
        List<ErrorMessage> response = validator.validateUpdateEvent(new Event(), DUMMY_GUID);
        assertEquals("Event ID is not exist.", response.get(0).getMessage());
    }

    @Test
    public void testValidateUpdateEventResponseFailed() {
        Event event = createEvent();
        when(eventService.getById(event.getId())).thenReturn(event);
        List<ErrorMessage> response = validator.validateUpdateEvent(new Event(), event.getId());
        assertEquals(10, response.size());
    }
}
