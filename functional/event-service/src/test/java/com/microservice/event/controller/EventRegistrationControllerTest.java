package com.microservice.event.controller;

import com.microservice.common.model.ErrorMessage;
import com.microservice.event.common.CommonTest;
import com.microservice.event.model.EventRegistration;
import com.microservice.event.security.filter.AWSCognitoIdTokenProcessor;
import com.microservice.event.service.EventRegistrationServiceImpl;
import com.microservice.event.validation.EventRegistrationValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventRegistrationControllerTest extends CommonTest {

    @InjectMocks
    private EventRegistrationController eventRegistrationController;

    @Mock
    private EventRegistrationServiceImpl eventRegistrationService;

    @Mock
    private HttpServletRequest request;

    @Mock
    private AWSCognitoIdTokenProcessor awsCognitoIdTokenProcessor;

    @Mock
    private EventRegistrationValidator validator;

    @Test
    public void testCreateEventReturnSuccess() throws Exception {
        ResponseEntity responseEntity = eventRegistrationController.registerEvent(request, "eventId");
        assertEquals(200, responseEntity.getStatusCode().value());
    }

    @Test
    public void testCreateEventReturnFailed() throws Exception {
        EventRegistration eventRegistration = createEventRegistration(false);

        List<ErrorMessage> errorMessages = new ArrayList<>();
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setCode(HttpStatus.NOT_ACCEPTABLE.toString());
        errorMessage.setMessage("Name");
        errorMessages.add(errorMessage);

        when(validator.validate(any(), anyString(), anyBoolean())).thenReturn(errorMessages);
        ResponseEntity responseEntity = eventRegistrationController.registerEvent(request, anyString());

        assertEquals(406, responseEntity.getStatusCode().value());

    }

    @Test
    public void testCancelEventReturnSuccess() throws Exception {
        ResponseEntity responseEntity = eventRegistrationController.cancelEvent(request, "eventId");
        assertEquals(200, responseEntity.getStatusCode().value());
    }
    
}
