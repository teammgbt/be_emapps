package com.microservice.event.security.filter;

import com.microservice.event.security.config.JwtAuthentication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 * @author Ryan_W737 on July 8, 2020
 */
@RunWith(MockitoJUnitRunner.class)
public class AWSCognitoJWTAuthFilterTest {
    private static final String USER_NAME = "emapps.mitrais@gmail.com";

    @InjectMocks
    private AWSCognitoJWTAuthFilter authFilter;

    @Mock
    private AWSCognitoIdTokenProcessor awsCognitoIdTokenProcessor;

    @Mock
    private ServletResponse response;

    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private FilterChain filterChain;

    @Test
    public void testDoFilterReturnSuccess() throws Exception {
        ServletRequest request = new HttpServletRequestWrapper(servletRequest);
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        User user = new User(USER_NAME, "", grantedAuthorities);
        Authentication authentication = new JwtAuthentication(user, null, new ArrayList<>());
        when(awsCognitoIdTokenProcessor.authenticate((HttpServletRequest) request)).thenReturn(authentication);
        authFilter.doFilter(request, response, filterChain);
    }

    @Test
    public void testDoFilterReturnException() throws Exception {
        ServletRequest request = new HttpServletRequestWrapper(servletRequest);
        when(awsCognitoIdTokenProcessor.authenticate((HttpServletRequest) request)).thenThrow(Exception.class);
        authFilter.doFilter(request, response, filterChain);
    }
}
