package com.microservice.event.common;

import com.microservice.event.model.Event;
import com.microservice.event.model.EventCategory;
import com.microservice.event.model.EventRegistration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Ryan_W737 on June 29, 2020
 */
public class CommonTest {
    private static final String DUMMY_GUID = UUID.randomUUID().toString();
    private static final String CATEGORY_NAME = "Outside event";

    public Event createEvent() {
        Event event = new Event();
        event.setId(DUMMY_GUID);
        event.setName("Event A");
        event.setCategoryName(CATEGORY_NAME);
        event.setTitle("Event A");
        event.setLocation(false);
        event.setDescription("Description");
        event.setImage("xxx");
        event.setStartDate(new Date());
        event.setEndDate(new Date());
        event.setVenueAddress("Singapore");
        event.setUserNote("Notes");
        event.setRegister(true);
        return event;
    }

    public EventCategory createEventCategory() {
        EventCategory eventCategory = new EventCategory();
        eventCategory.setId(DUMMY_GUID);
        eventCategory.setCategoryName(CATEGORY_NAME);
        eventCategory.setCategoryId(1);
        return eventCategory;
    }

    public List<Event> createListEvent() {
        List<Event> events = new ArrayList<>();
        Event event1 = new Event("name1", "venueAddress1", false, "categoryName1", "description1",
                "title1", "imageURL", new Date(), new Date(), "userNote1", true);
        events.add(event1);
        Event event2 = new Event("name2", "venueAddress2", false, "categoryName2", "description2",
                "title2", "imageURL", new Date(), new Date(), "userNote2", true);
        events.add(event2);
        return events;
    }

    public EventRegistration createEventRegistration(boolean registered) {
        EventRegistration eventRegistration = new EventRegistration();
        eventRegistration.setEventId("eventId");
        eventRegistration.setEventName("eventName");
        eventRegistration.setRegistrationStatus(registered);
        eventRegistration.generateCommonEntity(eventRegistration, "engkitsr@gmail.com");

        return eventRegistration;
    }
}
