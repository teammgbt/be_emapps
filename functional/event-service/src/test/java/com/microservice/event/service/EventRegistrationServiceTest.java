package com.microservice.event.service;

import com.microservice.event.common.CommonTest;
import com.microservice.event.model.Event;
import com.microservice.event.model.EventRegistration;
import com.microservice.event.repository.EventRegistrationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventRegistrationServiceTest extends CommonTest {

    @InjectMocks
    private EventRegistrationServiceImpl service;

    @Mock
    private EventServiceImpl eventService;

    @Mock
    private EventRegistrationRepository repository;

    @Mock
    private MongoTemplate mongoTemplate;

    @Test
    public void testFindEventRegistrationByUsernameAndEventIdReturnSuccess() {
        EventRegistration eventRegistration = createEventRegistration(true);

        when(mongoTemplate.findOne(any(Query.class), ArgumentMatchers.<Class<EventRegistration>>any())).thenReturn(eventRegistration);
        EventRegistration result = service.findEventRegistrationByUsernameAndEventId(eventRegistration.getEventName(), eventRegistration.getEventId());
        assertEquals(result.getEventName(), "eventName");
    }

    @Test
    public void testInsertEventRegistrationReturnSuccess() {
        EventRegistration eventRegistration = createEventRegistration(true);
        Event event = createEvent();

        when(repository.insert(any(EventRegistration.class))).thenReturn(eventRegistration);
        when(eventService.getById(anyString())).thenReturn(event);
        EventRegistration result = service.createEventRegistration(eventRegistration.getCreatedBy(), eventRegistration.getEventId());
        assertEquals(result.getEventName(), "eventName");
    }

    @Test
    public void testCancelEventReturnSuccess() {
        EventRegistration eventRegistration = createEventRegistration(true);

        when(mongoTemplate.findOne(any(Query.class), ArgumentMatchers.<Class<EventRegistration>>any())).thenReturn(eventRegistration);
        when(repository.save(any(EventRegistration.class))).thenReturn(eventRegistration);
        EventRegistration result = service.cancelEventRegistration(eventRegistration.getCreatedBy(), eventRegistration.getEventId());
        assertEquals(result.getEventName(), "eventName");
    }

}
