package com.microservice.event.security.config;

import com.microservice.event.security.filter.AWSCognitoJWTAuthFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ryan_W737 on July 8, 2020
 */
@RunWith(MockitoJUnitRunner.class)
public class SecurityConfigurationTest {
    @InjectMocks
    private SecurityConfiguration securityConfiguration;

    @Mock
    private AWSCognitoJWTAuthFilter filter;

    @Test
    public void testConfigureReturnSuccess() throws Exception {
        ObjectPostProcessor<Object> objectPostProcessor = Mockito.mock(ObjectPostProcessor.class);
        AuthenticationManagerBuilder authenticationBuilder = new AuthenticationManagerBuilder(objectPostProcessor);
        Map<Class<? extends Object>, Object> sharedObjects = new HashMap<>();
        HttpSecurity security = new HttpSecurity(objectPostProcessor, authenticationBuilder, sharedObjects);
        securityConfiguration.configure(security);
    }
}
