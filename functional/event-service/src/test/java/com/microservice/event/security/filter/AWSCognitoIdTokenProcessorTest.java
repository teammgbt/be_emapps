package com.microservice.event.security.filter;

import com.microservice.event.security.config.JwtAuthentication;
import com.microservice.event.security.config.JwtConfiguration;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @author Ryan_W737 on July 8, 2020
 */
@RunWith(MockitoJUnitRunner.class)
public class AWSCognitoIdTokenProcessorTest {
    private static final String POOL_URL = "https://cognito-idp.%s.amazonaws.com/%s";
    private static final String ID_TOKEN = "21123sadsad4565156qwewq";
    private static final String USERNAME = "username";

    @InjectMocks
    private AWSCognitoIdTokenProcessor processor;

    @Mock
    private JwtConfiguration jwtConfiguration;

    @Mock
    private ConfigurableJWTProcessor configurableJWTProcessor;

    @Mock
    private HttpServletRequest request;

    @Test
    public void testAuthenticateReturnSuccess() throws Exception {
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .issuer(POOL_URL)
                .claim(JwtConfiguration.USERNAME, USERNAME)
                .claim(JwtConfiguration.GROUP_FIELD, Collections.singletonList("groupField"))
                .build();
        when(request.getHeader(JwtConfiguration.HTTP_HEADER)).thenReturn(ID_TOKEN);
        when(jwtConfiguration.getCognitoIdentityPoolUrl()).thenReturn(POOL_URL);
        when(configurableJWTProcessor.process(anyString(), any())).thenReturn(claimsSet);
        Authentication authentication = processor.authenticate(request);
        assertEquals(((JwtAuthentication) authentication).getJwtClaimsSet(), claimsSet);
    }

    @Test
    public void testAuthenticateInvalidIssuerReturnException() throws Exception {
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .issuer("url")
                .build();
        when(request.getHeader(JwtConfiguration.HTTP_HEADER)).thenReturn(ID_TOKEN);
        when(jwtConfiguration.getCognitoIdentityPoolUrl()).thenReturn(POOL_URL);
        when(configurableJWTProcessor.process(anyString(), any())).thenReturn(claimsSet);
        try {
            processor.authenticate(request);
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Issuer url does not match cognito idp https://cognito-idp.%s.amazonaws.com/%s");
        }
    }

    @Test
    public void testAuthenticateNullHeaderReturnSuccess() throws Exception {
        Authentication authentication = processor.authenticate(request);
        assertNull(authentication);
    }

    @Test
    public void testGetUserNameReturnSuccess() throws ParseException, JOSEException, BadJOSEException {
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .issuer(POOL_URL)
                .claim(JwtConfiguration.USERNAME, USERNAME)
                .claim(JwtConfiguration.GROUP_FIELD, Collections.singletonList("groupField"))
                .build();
        when(configurableJWTProcessor.process(anyString(), any())).thenReturn(claimsSet);
        String result = processor.getUserName(ID_TOKEN);
        assertEquals(result, "username");
    }
}
