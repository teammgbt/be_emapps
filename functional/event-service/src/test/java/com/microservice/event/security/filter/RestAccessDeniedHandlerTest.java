package com.microservice.event.security.filter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.access.AccessDeniedException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Ryan_W737 on July 8, 2020
 */
@RunWith(MockitoJUnitRunner.class)
public class RestAccessDeniedHandlerTest {
    private static final String ACCESS_DENIED = "Access Denied";

    @InjectMocks
    private RestAccessDeniedHandler handler;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Test
    public void testHandleReturnSuccess() throws IOException {
        HttpServletRequest request = new HttpServletRequestWrapper(httpServletRequest);
        HttpServletResponse response = new HttpServletResponseWrapper(httpServletResponse);
        PrintWriter writer = new PrintWriter(ACCESS_DENIED);
        Mockito.when(response.getWriter()).thenReturn(writer);
        AccessDeniedException accessDeniedException = new AccessDeniedException(ACCESS_DENIED);
        handler.handle(httpServletRequest, httpServletResponse, accessDeniedException);
    }
}
