package com.microservice.event.controller;

import com.microservice.common.model.ResponseWrapper;
import com.microservice.event.common.CommonTest;
import com.microservice.event.model.EventCategory;
import com.microservice.event.repository.EventCategoryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ryan_W737 on June 29, 2020
 */
@RunWith(MockitoJUnitRunner.class)
public class EventCategoryControllerTest extends CommonTest {
    @InjectMocks
    private EventCategoryController categoryController;

    @Mock
    private EventCategoryRepository service;

    @Test
    public void testCreateEventCategoryReturnSuccess() throws Exception {
        EventCategory eventCategory = createEventCategory();
        Mockito.when(service.insert(eventCategory)).thenReturn(eventCategory);
        ResponseEntity<ResponseWrapper> responseEntity = categoryController.createEventCategory(eventCategory);
        Assert.assertEquals(200, responseEntity.getStatusCode().value());
    }

    @Test
    public void testGetEventCategoryReturnSuccess() {
        List<EventCategory> eventCategories = new ArrayList<>();
        EventCategory eventCategory = createEventCategory();
        eventCategories.add(eventCategory);
        Mockito.when(service.findAll()).thenReturn(eventCategories);
        ResponseEntity<ResponseWrapper> responseEntity = categoryController.getEventCategory();
        Assert.assertEquals(200, responseEntity.getStatusCode().value());
    }
}
