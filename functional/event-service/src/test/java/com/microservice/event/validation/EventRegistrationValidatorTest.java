package com.microservice.event.validation;

import com.microservice.common.model.ErrorMessage;
import com.microservice.event.common.CommonTest;
import com.microservice.event.model.Event;
import com.microservice.event.model.EventRegistration;
import com.microservice.event.service.EventRegistrationService;
import com.microservice.event.service.EventService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventRegistrationValidatorTest extends CommonTest {

    @InjectMocks
    private EventRegistrationValidator validator;

    @Mock
    private EventRegistrationService eventRegistrationService;

    @Mock
    private EventService eventService;

    @Test
    public void testValidationReturnSuccess() {
        EventRegistration eventRegistration = createEventRegistration(false);
        Event event = createEvent();
        eventRegistration.setId("eventRegistrationId");
        when(eventService.getById(any())).thenReturn(event);
        when(eventRegistrationService.findEventRegistrationByUsernameAndEventId(any(), anyString())).thenReturn(eventRegistration);
        List<ErrorMessage> response = validator.validate(any(), anyString(), true);
        assertEquals(0, response.size());
    }

    @Test
    public void testValidationReturnEventNotFound() {
        EventRegistration eventRegistration = createEventRegistration(true);
        eventRegistration.setId("eventRegistrationId");
        when(eventService.getById(any())).thenReturn(null);
        List<ErrorMessage> response = validator.validate(any(), anyString(), true);
        assertEquals(1, response.size());
        assertEquals("Invalid Event ID", response.get(0).getCode());
    }

    @Test
    public void testValidationReturnAlreadyRegistered() {
        EventRegistration eventRegistration = createEventRegistration(true);
        Event event = createEvent();
        eventRegistration.setId("eventRegistrationId");
        when(eventService.getById(any())).thenReturn(event);
        when(eventRegistrationService.findEventRegistrationByUsernameAndEventId(any(), anyString())).thenReturn(eventRegistration);
        List<ErrorMessage> response = validator.validate(any(), anyString(), true);
        assertEquals(1, response.size());
        assertEquals("Register", response.get(0).getCode());
    }

    @Test
    public void testValidationReturnCancellationError() {
        EventRegistration eventRegistration = createEventRegistration(false);
        Event event = createEvent();
        eventRegistration.setId("eventRegistrationId");
        when(eventService.getById(any())).thenReturn(event);
        when(eventRegistrationService.findEventRegistrationByUsernameAndEventId(any(), anyString())).thenReturn(eventRegistration);
        List<ErrorMessage> response = validator.validate(any(), anyString(), false);
        assertEquals(1, response.size());
        assertEquals("Cancellation", response.get(0).getCode());
    }

}
