package com.microservice.event.service;

import com.microservice.event.model.Event;
import com.microservice.event.model.EventRegistration;
import com.microservice.event.repository.EventRegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class EventRegistrationServiceImpl implements EventRegistrationService {

    @Autowired
    private EventRegistrationRepository service;

    @Autowired
    private EventServiceImpl eventService;

    @Autowired
    private MongoTemplate mongoTemplate;

    public EventRegistration findEventRegistrationByUsernameAndEventId(String username, String eventId) {

        Query query = new Query();
        query.addCriteria(new Criteria().andOperator(
                Criteria.where("eventId").is(eventId),
                Criteria.where("createdBy").is(username)));

        return mongoTemplate.findOne(query, EventRegistration.class);
    }

    public EventRegistration createEventRegistration(String username, String eventId) {

        EventRegistration eventRegistrations = findEventRegistrationByUsernameAndEventId(username, eventId);

        if (eventRegistrations != null && !eventRegistrations.getRegistrationStatus()) {
            eventRegistrations.setRegistrationStatus(true);
            return service.save(eventRegistrations);
        }

        Event event = eventService.getById(eventId);

        EventRegistration eventRegistration = new EventRegistration();
        eventRegistration.setRegistrationStatus(true);
        eventRegistration.setEventId(eventId);
        eventRegistration.setEventName(event.getName());
        eventRegistration.generateCommonEntity(eventRegistration, username);

        return service.insert(eventRegistration);
    }

    public EventRegistration cancelEventRegistration(String username, String eventId) {

        EventRegistration eventRegistration = findEventRegistrationByUsernameAndEventId(username, eventId);
        eventRegistration.setRegistrationStatus(false);

        return service.save(eventRegistration);
    }

}
