package com.microservice.event.service;

import com.microservice.event.model.Event;

import java.util.List;

public interface EventService {
    Event addEvent(Event event);

    List<Event> getAll();

    Event getById(String id);

    void deleteById(String id);

    Event editEvent(Event event, String username, String eventId);

    //get all event by created user
    List<Event> getAllByCreatedUserEmail(String userName, Integer pageNo, Integer pageSize);

    //Set event to cancelled
    Event setAsCancelled(String userName, String id);

    //get all active event
    List<Event> getAllEvent(Integer pageNo, Integer pageSize);

    //search event by keyword
    List<Event> searchEventByKeyword(String keyword, Integer pageNo, Integer pageSize);

    //search event by filter
    List<Event> getAllFilteredEvent(String category, Integer pageNo, Integer pageSize);
}