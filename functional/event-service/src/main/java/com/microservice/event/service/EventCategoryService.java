package com.microservice.event.service;

import com.microservice.event.model.EventCategory;

public interface EventCategoryService {
    EventCategory findByName(String name);
}
