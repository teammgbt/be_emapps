package com.microservice.event.service;

import com.microservice.event.model.Event;
import com.microservice.event.model.EventStatus;
import com.microservice.event.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {
    private static final String CREATED_DATE = "createdDate";

    @Autowired
    private EventRepository service;

    @Autowired
    private MongoTemplate mongoTemplate;

    public Event addEvent(Event event) {
        return service.insert(event);
    }

    public List<Event> getAll() {
        return service.findAll();
    }

    public Event getById(String id) {
        Optional<Event> event = service.findById(id);
        return event.orElse(null);
    }

    public void deleteById(String id) {
        service.deleteById(id);
    }

    public Event editEvent(Event event, String username, String eventId) {
        Event eventUpdated = getById(eventId);
        eventUpdated.generateUpdateCommonEntity(eventUpdated, username);
        eventUpdated.setName(event.getName());
        eventUpdated.setLocation(event.isLocation());
        eventUpdated.setCategoryName(event.getCategoryName());
        eventUpdated.setTitle(event.getTitle());
        eventUpdated.setDescription(event.getDescription());
        eventUpdated.setImage(event.getImage());
        eventUpdated.setStartDate(event.getStartDate());
        eventUpdated.setEndDate(event.getEndDate());
        eventUpdated.setUserNote(event.getUserNote());
        eventUpdated.setRegister(event.isRegister());
        eventUpdated.setVenueAddress(event.getVenueAddress());
        return service.save(eventUpdated);
    }

    public List<Event> getAllByCreatedUserEmail(String userName, Integer pageNo, Integer pageSize) {

        Query query = new Query(Criteria.where("createdBy").is(userName))
                .with(PageRequest.of(pageNo, pageSize))
                //default sort by CreatedDate for now
                .with(Sort.by(Sort.Direction.DESC, CREATED_DATE));
        return mongoTemplate.find(query, Event.class);
    }

    public Event setAsCancelled(String userName, String id) {
        Event eventCancel = getById(id);

        eventCancel.setStatusId(EventStatus.CANCELLED.id);
        eventCancel.generateUpdateCommonEntity(eventCancel, userName);
        service.save(eventCancel);

        return eventCancel;
    }

    @Override
    public List<Event> getAllEvent(Integer pageNo, Integer pageSize) {
        Query query = new Query(Criteria.where("statusId").is(1))
                .with(PageRequest.of(pageNo, pageSize))
                .with(Sort.by(Sort.Direction.DESC, CREATED_DATE));
        return mongoTemplate.find(query, Event.class);
    }

    @Override
    public List<Event> getAllFilteredEvent(String category, Integer pageNo, Integer pageSize) {
        Query query = new Query();
        query.addCriteria(new Criteria().andOperator(
                Criteria.where("statusId").is(1),
                Criteria.where("categoryName").regex("^"+category+"$", "i")))
                .with(PageRequest.of(pageNo, pageSize))
                .with(Sort.by(Sort.Direction.DESC, CREATED_DATE));
        return mongoTemplate.find(query, Event.class);
    }

    @Override
    public List<Event> searchEventByKeyword(String keyword, Integer pageNo, Integer pageSize) {
        Query query = new Query();
        query.addCriteria(new Criteria().orOperator(
                Criteria.where("categoryName").regex(keyword, "i"),
                Criteria.where("title").regex(keyword, "i"),
                Criteria.where("description").regex(keyword, "i"),
                Criteria.where("name").regex(keyword, "i")))
                .with(PageRequest.of(pageNo, pageSize))
                .with(Sort.by(Sort.Direction.ASC, CREATED_DATE));
        return mongoTemplate.find(query, Event.class);
    }
}
