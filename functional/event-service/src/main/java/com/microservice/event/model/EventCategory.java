package com.microservice.event.model;

import com.microservice.common.model.CommonEntity;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@ToString
@Document(collection = "eventCategory")
public class EventCategory extends CommonEntity {

    public EventCategory() {
        super();
    }

    @Id
    private String id;
    private int categoryId;
    private String categoryName;

    public void generateCommonEntity(EventCategory eventCategory) {
        Date date = new Date();
        eventCategory.setCreatedBy("System");
        eventCategory.setCreatedDate(date);
        eventCategory.setUpdatedBy("System");
        eventCategory.setUpdatedDate(date);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
