package com.microservice.event.model;

import com.microservice.common.model.CommonEntity;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
@Document(collection = "eventRegistration")
public class EventRegistration extends CommonEntity implements Serializable {

    public EventRegistration() { super(); }

    @Id
    private String id;
    private String eventId;
    private String eventName;
    private boolean registered;

    public void generateCommonEntity(EventRegistration eventRegistration, String userName) {
        Date date = new Date();
        eventRegistration.setCreatedBy(userName);
        eventRegistration.setCreatedDate(date);
        eventRegistration.setUpdatedBy(userName);
        eventRegistration.setUpdatedDate(date);
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getEventId() { return eventId; }

    public void setEventId(String eventId) { this.eventId = eventId; }

    public String getEventName() { return eventName; }

    public void setEventName(String eventName) { this.eventName = eventName; }

    public boolean getRegistrationStatus() { return registered; }

    public void setRegistrationStatus(boolean registered) { this.registered = registered; }

}
