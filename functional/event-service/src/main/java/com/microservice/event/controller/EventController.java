package com.microservice.event.controller;

import com.microservice.common.CommonResource;
import com.microservice.common.model.ErrorMessage;
import com.microservice.common.model.ResponseWrapper;
import com.microservice.event.model.Event;
import com.microservice.event.service.EventServiceImpl;
import com.microservice.event.validation.EventValidator;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.proc.BadJOSEException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

@RestController
public class EventController extends CommonResource {

    @Autowired
    private EventServiceImpl service;

    @Autowired
    private EventValidator validation;

    @PostMapping("/createEvent")
    public ResponseEntity<ResponseWrapper> createEvent(HttpServletRequest request, @RequestBody Event event)
            throws ParseException, JOSEException, BadJOSEException {
        String username = getUsernameByIdToken(request);

        List<ErrorMessage> errorMessages = validation.validate(event);
        if (!errorMessages.isEmpty()) {
            return new ResponseEntity(createFailedResponse(errorMessages), HttpStatus.NOT_ACCEPTABLE);
        }
        event.generateCommonEntity(event, username);
        Event result = service.addEvent(event);
        return ResponseEntity.ok(createOkResponse(result));
    }

    @PostMapping("/setAsCancelled")
    public ResponseEntity<ResponseWrapper> setAsCancelled(HttpServletRequest request, @RequestParam String eventId)
            throws ParseException, JOSEException, BadJOSEException {
        //validate eventID
        List<ErrorMessage> errorMessages = validation.validateEventSetAsCancelled(eventId);
        if(!errorMessages.isEmpty()){
            return new ResponseEntity(createFailedResponse(errorMessages), HttpStatus.NOT_ACCEPTABLE);
        }

        String username = getUsernameByIdToken(request);

        Event result = service.setAsCancelled(username, eventId);
        return ResponseEntity.ok(createOkResponse(result));
    }

    @GetMapping("/getMyCreatedEvents")
    public ResponseEntity<ResponseWrapper> getMyCreatedEvents(HttpServletRequest request,
            @RequestParam(defaultValue = "0") Integer pageNo, @RequestParam(defaultValue = "50") Integer pageSize)
            throws ParseException, JOSEException, BadJOSEException {
        String username = getUsernameByIdToken(request);

        List<Event> result = service.getAllByCreatedUserEmail(username, pageNo, pageSize);
        return ResponseEntity.ok(createOkResponse(result));
    }

    @PutMapping("/updateEvent/{eventId}")
    public ResponseEntity<ResponseWrapper> updateEvent(HttpServletRequest request, @PathVariable String eventId,
            @RequestBody Event event) throws ParseException, JOSEException, BadJOSEException {
        String username = getUsernameByIdToken(request);

        List<ErrorMessage> errorMessages = validation.validateUpdateEvent(event, eventId);
        if (!errorMessages.isEmpty()) {
            return new ResponseEntity(createFailedResponse(errorMessages), HttpStatus.NOT_ACCEPTABLE);
        }
        Event result = service.editEvent(event, username, eventId);
        return ResponseEntity.ok(createOkResponse(result));
    }

    @GetMapping("/getListEvents")
    public ResponseEntity<ResponseWrapper> getListEvents(
            @RequestParam(required = false) String categoryName,
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "50") Integer pageSize) {
        if (categoryName == null || categoryName.isEmpty()) {
            final List<Event> result = service.getAllEvent(pageNo, pageSize);
            return ResponseEntity.ok(createOkResponse(result));
        }
        final List<Event> result = service.getAllFilteredEvent(categoryName, pageNo, pageSize);
        return ResponseEntity.ok(createOkResponse(result));
    }

    @GetMapping("/searchEvent/{keyword}")
    public ResponseEntity<ResponseWrapper> searchEvent(@PathVariable String keyword,
            @RequestParam(defaultValue = "0") Integer pageNo, @RequestParam(defaultValue = "50") Integer pageSize) {
        List<Event> events = service.searchEventByKeyword(keyword, pageNo, pageSize);
        return ResponseEntity.ok(createOkResponse(events));
    }

    @GetMapping("/getEventDetail/{eventId}")
    public ResponseEntity<ResponseWrapper> getEventDetail(@PathVariable String eventId) {
        Event events = service.getById(eventId);
        return ResponseEntity.ok(createOkResponse(events));
    }
}
