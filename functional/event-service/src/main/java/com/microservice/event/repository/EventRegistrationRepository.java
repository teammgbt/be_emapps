package com.microservice.event.repository;

import com.microservice.event.model.EventRegistration;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EventRegistrationRepository extends MongoRepository<EventRegistration, String> {
}
