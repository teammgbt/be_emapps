package com.microservice.event.service;

import com.microservice.event.model.EventCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class EventCategoryServiceImpl implements EventCategoryService {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public EventCategory findByName(String name) {
        Query query = new Query(Criteria.where("categoryName").is(name));
        return mongoTemplate.findOne(query, EventCategory.class);
    }
}
