package com.microservice.event.validation;

import com.microservice.event.model.Event;
import com.microservice.event.model.EventCategory;
import com.microservice.event.model.EventStatus;
import com.microservice.common.model.ErrorMessage;
import com.microservice.event.service.EventCategoryService;
import com.microservice.event.service.EventService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class EventValidator {

    @Autowired
    private EventCategoryService eventCategoryService;

    @Autowired
    private EventService eventService;

    public List<ErrorMessage> validate(Event event) {

        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (StringUtils.isBlank(event.getName())) {
            errorMessages.add(new ErrorMessage("Name", "Name must be set"));

        }
        if (StringUtils.isBlank(event.getCategoryName())) {
            errorMessages.add(new ErrorMessage("CategoryName", "Category must be set"));
        } else {
            EventCategory eventCategory = eventCategoryService.findByName(event.getCategoryName());
            if (eventCategory == null) {
                errorMessages.add(new ErrorMessage("CategoryName", "Category does not exist"));
            }
        }
        if (StringUtils.isBlank(event.getTitle())) {
            errorMessages.add(new ErrorMessage("Title", "Title must be set"));
        }
        if (StringUtils.isBlank(event.getDescription())) {
            errorMessages.add(new ErrorMessage("Description", "Description must be set"));
        }
        if (StringUtils.isBlank(event.getImage())) {
            errorMessages.add(new ErrorMessage("Image", "Image must be set"));
        }
        if (event.getStartDate() == null) {
            errorMessages.add(new ErrorMessage("StartDate", "Start Date must be set"));
        }
        if (event.getEndDate() == null) {
            errorMessages.add(new ErrorMessage("EndDate", "End Date must be set"));
        }
        if (StringUtils.isBlank(event.getVenueAddress())) {
            errorMessages.add(new ErrorMessage("VenueAddress", "Venue Address must be set"));
        }
        if (StringUtils.isBlank(event.getUserNote())) {
            errorMessages.add(new ErrorMessage("UserNote", "UserNote must be set"));
        }
        if (!event.isRegister()) {
            errorMessages.add(new ErrorMessage("Register", "Register must be checked"));
        }
        return errorMessages;
    }

    public List<ErrorMessage> validateEventSetAsCancelled(String eventId){

        Event optEvent = eventService.getById(eventId);

        return Stream.concat(validateEventIdExist(optEvent).stream(), validateAlreadyCancelled(optEvent).stream())
                .collect(Collectors.toList());
    }

    public List<ErrorMessage> validateUpdateEvent(Event event, String eventId) {

        Event optEvent = eventService.getById(eventId);

        return Stream.concat(validateEventIdExist(optEvent).stream(), validate(event).stream())
                .collect(Collectors.toList());
    }

    private List<ErrorMessage> validateAlreadyCancelled(Event eventOpt) {

        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (eventOpt != null && eventOpt.getStatusId() == EventStatus.CANCELLED.id) {
            errorMessages.add(new ErrorMessage("statusId", "Event Status already cancelled."));
        }

        return errorMessages;
    }

    private List<ErrorMessage> validateEventIdExist(Event eventOpt) {

        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (eventOpt == null) {
            errorMessages.add(new ErrorMessage("eventId","Event ID is not exist."));
        }

        return errorMessages;
    }
}
