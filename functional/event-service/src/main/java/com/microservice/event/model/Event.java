package com.microservice.event.model;

import com.microservice.common.model.CommonEntity;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
@Document(collection = "events")
public class Event extends CommonEntity implements Serializable {
    public Event(String name, String venueAddress, boolean location, String categoryName, String description,
                 String title, String image, Date startDate, Date endDate, String userNote, boolean register) {
        this.name = name;
        this.location = location;
        this.categoryName = categoryName;
        this.title = title;
        this.description = description;
        this.image = image;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userNote = userNote;
        this.register = register;
        this.venueAddress = venueAddress;
    }

    public Event() {
        super();
    }

    @Id
    private String id;
    private String name;
    private String categoryName;
    private String title;
    private String description;
    private String image;
    private Date startDate;
    private Date endDate;
    private String venueAddress;
    private boolean location;
    private String userNote;
    private boolean register;

    public void generateCommonEntity(Event event, String userName) {
        Date date = new Date();
        event.setCreatedBy(userName);
        event.setCreatedDate(date);
        event.setUpdatedBy(userName);
        event.setUpdatedDate(date);
    }

    public void generateUpdateCommonEntity(Event event, String userName) {
        Date date = new Date();
        event.setUpdatedBy(userName);
        event.setUpdatedDate(date);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getVenueAddress() {
        return venueAddress;
    }

    public void setVenueAddress(String venueAddress) {
        this.venueAddress = venueAddress;
    }

    public boolean isLocation() {
        return location;
    }

    public void setLocation(boolean location) {
        this.location = location;
    }

    public String getUserNote() {
        return userNote;
    }

    public void setUserNote(String userNote) {
        this.userNote = userNote;
    }

    public boolean isRegister() {
        return register;
    }

    public void setRegister(boolean register) {
        this.register = register;
    }
}
