package com.microservice.event.repository;

import com.microservice.event.model.EventCategory;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EventCategoryRepository extends MongoRepository<EventCategory, String> {
}
