package com.microservice.event.validation;

import com.microservice.event.model.Event;
import com.microservice.event.model.EventRegistration;
import com.microservice.common.model.ErrorMessage;
import com.microservice.event.service.EventRegistrationService;
import com.microservice.event.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventRegistrationValidator {

    @Autowired
    private EventService eventService;

    @Autowired
    private EventRegistrationService eventRegistrationService;

    public List<ErrorMessage> validate(String username, String eventId, boolean shouldCheckRegistrationStatus) {

        List<ErrorMessage> errorMessages = new ArrayList<>();

        EventRegistration eventRegistration = eventRegistrationService.findEventRegistrationByUsernameAndEventId(username, eventId);

        Event event = eventService.getById(eventId);

        if (event == null) {
            errorMessages.add(new ErrorMessage("Invalid Event ID", "The eventId you entered is invalid"));
            return errorMessages;
        } else if (eventRegistration != null && eventRegistration.getRegistrationStatus() && shouldCheckRegistrationStatus) {
            errorMessages.add(new ErrorMessage("Register", "The user has been registered"));
            return errorMessages;
        } else if (eventRegistration != null && !eventRegistration.getRegistrationStatus() && !shouldCheckRegistrationStatus) {
            errorMessages.add(new ErrorMessage("Cancellation", "The user has canceled this event"));
            return errorMessages;
        }

        return errorMessages;
    }

}
