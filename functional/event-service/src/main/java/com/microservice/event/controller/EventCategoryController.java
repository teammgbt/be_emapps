package com.microservice.event.controller;

import com.microservice.common.CommonResource;
import com.microservice.common.model.ResponseWrapper;
import com.microservice.event.model.EventCategory;
import com.microservice.event.repository.EventCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventCategoryController extends CommonResource {
    @Autowired
    private EventCategoryRepository service;

    @PostMapping("/createEventCategory")
    public ResponseEntity<ResponseWrapper> createEventCategory(@RequestBody EventCategory eventCategory) {
        eventCategory.generateCommonEntity(eventCategory);
        EventCategory result = service.insert(eventCategory);
        return ResponseEntity.ok(createOkResponse(result));
    }

    @GetMapping("/getAllEventCategory")
    public ResponseEntity<ResponseWrapper> getEventCategory() {
        return ResponseEntity.ok(createOkResponse(service.findAll()));
    }
}
