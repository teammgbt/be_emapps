package com.microservice.event.service;

import com.microservice.event.model.EventRegistration;

public interface EventRegistrationService {

    EventRegistration createEventRegistration(String username, String eventId);
    EventRegistration findEventRegistrationByUsernameAndEventId(String username, String eventId);
}
