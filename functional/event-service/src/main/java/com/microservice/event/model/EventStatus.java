package com.microservice.event.model;

public enum EventStatus {
    PUBLISHED(1),
    DRAFT(2),
    CANCELLED(3);

    public final int id;

    private EventStatus(int id) {
        this.id = id;
    }
}
