package com.microservice.event.controller;

import com.microservice.common.CommonResource;
import com.microservice.common.model.ErrorMessage;
import com.microservice.common.model.ResponseWrapper;
import com.microservice.event.model.EventRegistration;
import com.microservice.event.service.EventRegistrationServiceImpl;
import com.microservice.event.validation.EventRegistrationValidator;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.proc.BadJOSEException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

@RestController
public class EventRegistrationController extends CommonResource {

    @Autowired
    private EventRegistrationServiceImpl service;

    @Autowired
    private EventRegistrationValidator validator;

    @PostMapping("/registerEvent/{eventId}")
    public ResponseEntity<ResponseWrapper> registerEvent(HttpServletRequest request, @PathVariable String eventId) throws ParseException, JOSEException, BadJOSEException {

        String username = getUsernameByIdToken(request);

        List<ErrorMessage> errorMessages = validator.validate(username, eventId, true);

        if (!errorMessages.isEmpty()) {
            return new ResponseEntity(createFailedResponse(errorMessages), HttpStatus.NOT_ACCEPTABLE);
        }

        EventRegistration newEventRegistration = service.createEventRegistration(username, eventId);

        return ResponseEntity.ok(createOkResponse(newEventRegistration));
    }

    @PostMapping("/cancelEvent/{eventId}")
    public ResponseEntity<ResponseWrapper> cancelEvent(HttpServletRequest request, @PathVariable String eventId) throws ParseException, JOSEException, BadJOSEException {

        String username = getUsernameByIdToken(request);

        List<ErrorMessage> errorMessages = validator.validate(username, eventId, false);

        if (!errorMessages.isEmpty()) {
            return new ResponseEntity(createFailedResponse(errorMessages), HttpStatus.NOT_ACCEPTABLE);
        }

        EventRegistration newEventRegistration = service.cancelEventRegistration(username, eventId);

        return ResponseEntity.ok(createOkResponse(newEventRegistration));
    }
}
