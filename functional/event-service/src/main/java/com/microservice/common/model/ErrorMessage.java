package com.microservice.common.model;

import lombok.Data;

/**
 * Created by krisraya on 23/06/2020.
 * Error message class
 * 
 */
@Data
public class ErrorMessage {
	

	private String message;
	private String code;

	/**
	 * @param message
	 * @param code
	 */
	public ErrorMessage(String code, String message) {
		super();
		this.message = message;
		this.code = code;
	}

	public ErrorMessage(String message) {
		super();
		this.message = message;
		this.code = null;
	}

	public ErrorMessage() {
		super();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
