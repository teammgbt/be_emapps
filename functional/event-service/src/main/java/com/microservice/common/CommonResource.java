package com.microservice.common;

import com.microservice.common.model.ErrorMessage;
import com.microservice.common.model.ResponseWrapper;
import com.microservice.event.security.filter.AWSCognitoIdTokenProcessor;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.proc.BadJOSEException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

import static java.util.Collections.singletonMap;

public class CommonResource {

    @Autowired
    private AWSCognitoIdTokenProcessor awsCognitoIdTokenProcessor;

    private static final String AUTHORIZATION_KEY = "Authorization";
    private static final String STATUS_KEY = "status";

    public String getUsernameByIdToken(HttpServletRequest request) throws ParseException, JOSEException, BadJOSEException {
        String token = request.getHeader(AUTHORIZATION_KEY);
        return awsCognitoIdTokenProcessor.getUserName(token);
    }

    public ResponseWrapper createOkResponse(Object object) {
        return new ResponseWrapper(object, singletonMap(STATUS_KEY, HttpStatus.OK), null);
    }

    public ResponseWrapper createFailedResponse(List<ErrorMessage> errorMessages) {
        return new ResponseWrapper(null, singletonMap(STATUS_KEY, HttpStatus.NOT_ACCEPTABLE), errorMessages);
    }
}
