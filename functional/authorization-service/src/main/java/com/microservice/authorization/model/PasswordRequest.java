package com.microservice.authorization.model;


import lombok.Data;

import javax.validation.constraints.Email;

/**
 * Created by dendy-prtha on 02/04/2020.
 * class that contains the forgot password request data
 * 
 */

@Data
public class PasswordRequest {
	@Email(message = "Username Email should be valid email")
	private String username;

	private String newPassword;
	private String confirmationCode;
	private String oldPassword;
	private String accessToken;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmationCode() {
		return confirmationCode;
	}

	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
}
