package com.microservice.authorization.model;


import lombok.Data;

/**
 *
 * Created by dendy-prtha on 02/04/2020.
 * Class that contains the authentication request data.
 * 
 */
@Data
public class AuthenticationRequest {

    private String username;
    private String password;
    private String newPassword;
    private String accessToken;

    public AuthenticationRequest() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}