package com.microservice.authorization.model;

import lombok.Data;

/**
 * Created by dendy-prtha on 02/04/2020.
 * AWSUser.java class that contains the logic
 *  
 */
@Data
public class UserSignUpResponse {
    private String userCreatedDate;
    private String lastModifiedDate;
    private boolean enabled;
    private String userStatus;
    private String password;
    private String email;
    private String awsResponse;
    private String accessToken;
    private String idToken;
    private String userRole;

    public String getUserCreatedDate() {
        return userCreatedDate;
    }

    public void setUserCreatedDate(String userCreatedDate) {
        this.userCreatedDate = userCreatedDate;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAwsResponse() {
        return awsResponse;
    }

    public void setAwsResponse(String awsResponse) {
        this.awsResponse = awsResponse;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getUserRole() { return userRole; }

    public void setUserRole(String userRole) { this.userRole = userRole; }
}