package com.microservice.authorization.controller;

import com.microservice.authorization.model.AuthenticationResponse;
import com.microservice.authorization.model.UserRequest;
import com.microservice.authorization.security.model.ResponseWrapper;
import com.microservice.authorization.service.AWSCognitoAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Collections.singletonMap;

/**
 * Created by kris-raya on 07/20/2020.
 * SpringBoot Admin Controller
 */

@RestController
@RequestMapping("admin")
public class AdminController {
    @Autowired
    private AWSCognitoAuthService awsAuthService;

    /**
     * addUserToGroup
     *
     * @param userRequest
     * @return ResponseEntity<ResponseWrapper>
     */
    @PostMapping(value = "/addUserToGroup")
    public ResponseEntity<ResponseWrapper> addUserToGroup(@RequestBody UserRequest userRequest) {
        //Calls the service
        final String username = userRequest.getUsername();
        final String userRole = userRequest.getUserRole();
        awsAuthService.addUserToGroup(username, userRole);

        AuthenticationResponse response = new AuthenticationResponse();
        response.setMessage(String.format("User %s added to group %s", username, userRole));
        response.setUsername(username);

        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    /**
     * getGroupOfUser
     *
     * @param userRequest
     * @return ResponseEntity<ResponseWrapper>
     */
    @PostMapping(value = "/getGroupOfUser")
    public ResponseEntity<ResponseWrapper> getGroupOfUser(@RequestBody UserRequest userRequest) {
        //Calls the service
        final String username = userRequest.getUsername();
        final List<String> userRole = awsAuthService.getUserRole(username);

        AuthenticationResponse response = new AuthenticationResponse();
        response.setUsername(username);
        response.setUserRole(userRole);

        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    /**
     * removeUserFromGroup
     *
     * @param userRequest
     * @return ResponseEntity<ResponseWrapper>
     */
    @PostMapping(value = "/removeUserFromGroup")
    public ResponseEntity<ResponseWrapper> removeUserFromGroup(@RequestBody UserRequest userRequest) {
        //Calls the service
        final String username = userRequest.getUsername();
        final String userRole = userRequest.getUserRole();
        awsAuthService.removeUserFromGroup(username, userRole);

        AuthenticationResponse response = new AuthenticationResponse();
        response.setMessage(String.format("User %s removed from group %s", username, userRole));
        response.setUsername(username);

        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    private ResponseWrapper createOkResponse(Object object) {
        return new ResponseWrapper(
                object,
                singletonMap("status", HttpStatus.OK), null);
    }
}
