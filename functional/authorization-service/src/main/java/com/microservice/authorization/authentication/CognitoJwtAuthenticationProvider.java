package com.microservice.authorization.authentication;

import com.microservice.authorization.model.AuthenticationRequest;
import com.microservice.authorization.security.model.SpringSecurityUser;
import com.microservice.authorization.service.AWSCognitoAuthService;
import com.microservice.authorization.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dendy-prtha on 02/04/2020.
 * Custom authentication provider that will manage the cognito token authentication.
 */

@Component
public class CognitoJwtAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private AWSCognitoAuthService cognitoService;

    /* (non-Javadoc)
     * @see org.springframework.security.authentication.AuthenticationProvider#authenticate(org.springframework.security.core.Authentication)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Authentication authenticate(Authentication authentication) {
        if (null != authentication) {
            AuthenticationRequest authenticationRequest = new AuthenticationRequest();
            Map<String, String> credentials = (Map<String, String>) authentication.getCredentials();
            authenticationRequest.setNewPassword(credentials.get(Constants.NEW_PASS_WORD_KEY));
            authenticationRequest.setPassword(credentials.get(Constants.PASS_WORD_KEY));
            authenticationRequest.setUsername(authentication.getName());
            SpringSecurityUser userAuthenticated = cognitoService.authenticate(authenticationRequest);
            if (null != userAuthenticated) {
                // use the credentials
                // and authenticate against the third-party system
                Map<String, String> authenticatedCredentials = new HashMap<>();
                authenticatedCredentials.put(Constants.ACCESS_TOKEN_KEY, userAuthenticated.getAccessToken());
                authenticatedCredentials.put(Constants.EXPIRES_IN_KEY, userAuthenticated.getExpiresIn().toString());
                authenticatedCredentials.put(Constants.ID_TOKEN_KEY, userAuthenticated.getIdToken());
                authenticatedCredentials.put(Constants.PASS_WORD_KEY, userAuthenticated.getPassword());
                authenticatedCredentials.put(Constants.REFRESH_TOKEN_KEY, userAuthenticated.getRefreshToken());
                authenticatedCredentials.put(Constants.TOKEN_TYPE_KEY, userAuthenticated.getTokenType());
                return new UsernamePasswordAuthenticationToken(
                        userAuthenticated.getUsername(), authenticatedCredentials, userAuthenticated.getAuthorities());
            } else {
                return null;
            }
        } else {
            throw new UsernameNotFoundException(String.format("No appUser found with username '%s'.", ""));
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
                UsernamePasswordAuthenticationToken.class);
    }
}
