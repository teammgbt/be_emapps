package com.microservice.authorization.authentication;

/**
 * Created by dendy-prtha on 02/04/2020.
 * Bean that holds the IDToken associated with a specify user.
 * 
 * @version
 * 
 */
public class CognitoJwtIdTokenCredentialsHolder {

	private String idToken;
	
    public String getIdToken() {
        return idToken;
    }

    public CognitoJwtIdTokenCredentialsHolder setIdToken(String idToken) {
        this.idToken = idToken;
        return this;
    }

   
}
