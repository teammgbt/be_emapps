package com.microservice.authorization.model;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by dendy-prtha on 02/04/2020.
 *
 * sign up POJO request
 */
@Data
public class UserSignUpRequest {
    @NotNull(message = "Address should not be Null")
    @NotEmpty(message = "Address should not be empty")
    private String address;

    @NotNull(message = "Birthdate should not be Null")
    @NotEmpty(message = "Birthdate should not be empty")
    private String birthdate;

    @NotNull(message = "Email should not be Null")
    @NotEmpty(message = "Email should not be empty")
    @Email(message = "Email Should be valid")
    private String email;

    @NotNull(message = "Gender should not be Null")
    @NotEmpty(message = "Gender should not be empty")
    private String gender;

    @Size(min = 6, max = 20, message
            = "Password must be between 6 and 20 characters")
    private String password;

    @NotNull(message = "Middle Name should not be Null")
    @NotEmpty(message = "Middle Name should not be empty")
    private String middleName;

    @NotNull(message = "Name should not be Null")
    @NotEmpty(message = "Name should not be empty")
    private String name;

    private boolean organizer;

    public boolean isOrganizer() { return organizer; }

    public void setOrganizer(boolean organizer) { this.organizer = organizer; }
}
