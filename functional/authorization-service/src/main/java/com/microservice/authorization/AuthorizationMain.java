package com.microservice.authorization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * Created by dendy-prtha on 02/04/2020.
 *
 */

@SpringBootApplication
public class AuthorizationMain {

	public static void main(String[] args) {
		SpringApplication.run(AuthorizationMain.class, args);
	}
}

