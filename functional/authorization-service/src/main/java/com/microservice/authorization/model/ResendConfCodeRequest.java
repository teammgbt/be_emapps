package com.microservice.authorization.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ResendConfCodeRequest {

    @NotNull(message = "Username should not be Null")
    @NotEmpty(message = "Username should not be empty")
    private String username;
}
