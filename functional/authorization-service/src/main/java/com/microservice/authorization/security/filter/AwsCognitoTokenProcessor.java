package com.microservice.authorization.security.filter;

import com.microservice.authorization.exception.CognitoException;
import com.microservice.authorization.model.UserResponse;
import com.microservice.authorization.security.config.CognitoJwtAuthentication;
import com.microservice.authorization.service.AWSCognitoAuthService;
import com.microservice.authorization.util.AWSConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by dendy-prtha on 02/04/2020.
 * Method that Validates the AWS Cognito ID Token.
 *To verify JWT claims:
        *1.Verify that the token is not expired.
        *2.The audience(aud)claim should match the app client ID created in the Amazon Cognito user pool.
        *3.The issuer(iss)claim should match your user pool.For example,a user pool created in the us-east-1region will have an iss value of:https://cognito-idp.us-east-1.amazonaws.com/<userpoolID>.
        *4.Check the token_use claim.
        *5.If you are only accepting the access token in your web APIs,its value must be access.
        *6.If you are only using the ID token,its value must be id.
        *7.If you are using both ID and access tokens,the token_use claim must be either id or access.
        *8.You can now trust the claims inside the token.

 * @version 1.0
 */
@Component
public class AwsCognitoTokenProcessor {


	private static final String INVALID_TOKEN = "Invalid Token";

	private static final String ROLE_PREFIX = "ROLE_";
	private static final String EMPTY_STRING = "";

	@Autowired
	private ConfigurableJWTProcessor configurableJWTProcessor;

	@Autowired
	private AWSConfig jwtConfiguration;

    @Autowired
	private AWSCognitoAuthService authService;

	/**
	 * Method that verifies if the token has the Bearer string key and if it does it removes it.
	 *@param token
	 *@return token - without the Bearer string
	 *@throws ParseException
	 * 
	 *  
	 */
	private String extractAndDecodeJwt(String token) {
		String tokenResult = token;

		if (token != null && token.startsWith("Bearer ")) {
			tokenResult = token.substring("Bearer ".length());
		}
		return tokenResult;
	}

	/**
	 * Method that obtains the authentication and validates the JWT ClaimsSet defined using ID token
     *
	 *@param request
	 *@return org.springframework.security.core.Authentication
	 * @throws ParseException 
	 * @throws JOSEException 
	 * @throws BadJOSEException 
	 *@throws Exception
	 * 
	 *
     *
	 */
	public Authentication getIdAuthentication(HttpServletRequest request) throws ParseException, BadJOSEException, JOSEException {
		String idToken = request.getHeader(AWSConfig.HTTP_HEADER);
		if (null != idToken) {
			idToken = extractAndDecodeJwt(idToken);
			JWTClaimsSet claimsSet;

			claimsSet = configurableJWTProcessor.process(idToken, null);

			if (!isIssuedCorrectly(claimsSet)) {
				throw new CognitoException(INVALID_TOKEN,CognitoException.INVALID_TOKEN_EXCEPTION_CODE, String.format("Issuer %s in JWT token doesn't match cognito idp %s", claimsSet.getIssuer(), jwtConfiguration.getCognitoIdentityPoolUrl()));
			}

			if (!isIdToken(claimsSet)) {
				throw new CognitoException(INVALID_TOKEN,CognitoException.NOT_A_TOKEN_EXCEPTION, "JWT Token doesn't seem to be an ID Token");
			}

			String username = claimsSet.getClaims().get(AWSConfig.USERNAME).toString();

			List<String> groups = (List<String>) claimsSet.getClaims().get(AWSConfig.GROUP_FIELD);
			List<GrantedAuthority> grantedAuthorities = convertList(groups, group -> new SimpleGrantedAuthority(ROLE_PREFIX + group.toUpperCase()));
			User user = new User(username, EMPTY_STRING, grantedAuthorities);

			return new CognitoJwtAuthentication(user, claimsSet, grantedAuthorities);
		}
		return null;
	}

    /**
     * Method that obtains the authentication and validates the JWT ClaimsSet defined using session token
     *
     *@param request
     *@return org.springframework.security.core.Authentication
     * @throws ParseException
     * @throws JOSEException
     * @throws BadJOSEException
     *@throws Exception
     *
     *
     *
     */
    public Authentication getSessionAuthentication(HttpServletRequest request) throws ParseException, BadJOSEException, JOSEException {
        String token = request.getHeader(AWSConfig.HTTP_HEADER);
        if (null != token) {
            token = extractAndDecodeJwt(token);
            JWTClaimsSet claimsSet;

            claimsSet = configurableJWTProcessor.process(token, null);

            if (!isIssuedCorrectly(claimsSet)) {
                throw new CognitoException(INVALID_TOKEN,CognitoException.INVALID_TOKEN_EXCEPTION_CODE, String.format("Issuer %s in JWT token doesn't match cognito idp %s", claimsSet.getIssuer(), jwtConfiguration.getCognitoIdentityPoolUrl()));
            }

            if (!isAccessToken(claimsSet)) {
                throw new CognitoException(INVALID_TOKEN,CognitoException.NOT_A_TOKEN_EXCEPTION, "JWT Token doesn't seem to be an Access Token");
            }

            UserResponse userResponse = authService.getUserInfo(token);
            List<String> groups = (List<String>) claimsSet.getClaims().get(AWSConfig.GROUP_FIELD);
            List<GrantedAuthority> grantedAuthorities = convertList(groups, group -> new SimpleGrantedAuthority(ROLE_PREFIX + group.toUpperCase()));
            User user = new User(userResponse.getUsername(), EMPTY_STRING, grantedAuthorities);
            return new CognitoJwtAuthentication(user, claimsSet, grantedAuthorities);
        }
        return null;
    }

	/**
	 * Method that validates if the tokenId is issued correctly.
	 *@param claimsSet
	 *@return boolean
	 * 
	 *  
	 */
	private boolean isIssuedCorrectly(JWTClaimsSet claimsSet) {
		return claimsSet.getIssuer().equals(jwtConfiguration.getCognitoIdentityPoolUrl());
	}


	/**
	 * Method that validates if the ID token is valid.
	 *@param claimsSet
	 *@return
	 * 
	 *  
	 */
	private boolean isIdToken(JWTClaimsSet claimsSet) {
		return claimsSet.getClaim("token_use").equals("id");
	}

	/**
	 * Method that validates if the Session/Access token is valid.
	 *@param claimsSet
	 *@return
	 *
	 *
	 */
	private boolean isAccessToken(JWTClaimsSet claimsSet) {
		return claimsSet.getClaim("token_use").equals("access");
	}


	/**
	 * Method generics.
	 *@param from
	 *@param func
	 *@return
	 * 
	 *  
	 */
	private static <T, U> List<U> convertList(List<T> from, Function<T, U> func) {
		return from.stream().map(func).collect(Collectors.toList());
	}
}