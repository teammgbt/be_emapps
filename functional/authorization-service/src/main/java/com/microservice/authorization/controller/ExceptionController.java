package com.microservice.authorization.controller;

import com.microservice.authorization.exception.CognitoException;
import com.microservice.authorization.security.model.ErrorMessage;
import com.microservice.authorization.security.model.ResponseWrapper;
import com.microservice.authorization.security.model.RestErrorList;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonMap;

/**
 * Created by dendy-prtha on 02/04/2020.
 * Exception end point controller used to do exception response
 */
@ControllerAdvice
@EnableWebMvc
public class ExceptionController extends ResponseEntityExceptionHandler {

    private static final String STATUS = "status";
    protected ResponseEntity<Object> handleExceptionInternal(@NotNull Exception ex) {
        RestErrorList errorList = new RestErrorList(HttpStatus.NOT_ACCEPTABLE, new ErrorMessage(ex.getMessage(),
                HttpStatus.INTERNAL_SERVER_ERROR.toString(), ex.getCause().toString()));
        return new ResponseEntity(new ResponseWrapper(null,
                singletonMap(STATUS, HttpStatus.NOT_ACCEPTABLE), errorList), HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     * handleMethodArgumentNotValid - Handles all argument not valid exception.
     *
     * @param ex
     * @return ResponseEntity<ResponseWrapper>
     */
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        for (ObjectError error : ex.getBindingResult().getAllErrors()) {
            errorMessages.add(new ErrorMessage(error.getDefaultMessage(),HttpStatus.NOT_ACCEPTABLE.toString() , ""));
        }
        RestErrorList errorList = new RestErrorList(HttpStatus.BAD_REQUEST.value(), errorMessages);
        return new ResponseEntity(new ResponseWrapper(null,
                singletonMap(STATUS, HttpStatus.NOT_ACCEPTABLE), errorList), HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     * handleException - Handles all the Exception recieving a request, responseWrapper.
     *
     * @param responseWrapper
     * @return ResponseEntity<ResponseWrapper>
     */
    @ExceptionHandler(Exception.class)
    public @ResponseBody
    ResponseEntity<ResponseWrapper> handleException(ResponseWrapper responseWrapper) {
        return new ResponseEntity(responseWrapper, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * handleIOException - Handles all the Authentication Exceptions of the application.
     *
     * @param exception
     * @return ResponseEntity<ResponseWrapper>
     */
    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<ResponseWrapper> handleIOException(CognitoException exception) {
        RestErrorList errorList = new RestErrorList(HttpStatus.NOT_ACCEPTABLE,
                new ErrorMessage(exception.getErrorMessage(), exception.getErrorCode(),
                        exception.getDetailErrorMessage()));
        return new ResponseEntity(new ResponseWrapper(null, singletonMap(STATUS, HttpStatus.NOT_ACCEPTABLE),
                errorList), HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     * handleJwtException - Handles all the JWT Exceptions of the application.
     *
     * @param exception
     * @return ResponseEntity<ResponseWrapper>
     */
    public ResponseWrapper handleJwtException(CognitoException exception) {

        RestErrorList errorList = new RestErrorList(HttpStatus.UNAUTHORIZED, new ErrorMessage(
                exception.getErrorMessage(), exception.getErrorCode(), exception.getDetailErrorMessage()));
        return new ResponseWrapper(null, singletonMap(STATUS, HttpStatus.UNAUTHORIZED), errorList);
    }

}