package com.microservice.authorization.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Created by dendy-prtha on 02/04/2020.
 * PasswordResponse.java class that contains the logic
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class PasswordResponse {
	
	private String destination;
	private String deliveryMedium;
	private String message;
	private String username;

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDeliveryMedium() {
		return deliveryMedium;
	}

	public void setDeliveryMedium(String deliveryMedium) {
		this.deliveryMedium = deliveryMedium;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
