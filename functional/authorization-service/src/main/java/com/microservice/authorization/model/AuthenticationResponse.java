package com.microservice.authorization.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * Created by dendy-prtha on 02/04/2020.
 * Method that contains the Authentication Response Data
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class AuthenticationResponse {

    private String idToken;
    private String accessToken;
    private String refreshToken;
    private String expiresIn;
    private String actualDate;
    private String expirationDate;
    private UserResponse userData;
    private String username;
    private String message;
    private List<String> userRole;

    public AuthenticationResponse() {
        super();
    }

    public AuthenticationResponse(String idToken, String sessionToken, String refreshToken, String expiresIn, UserResponse userData, List<String> userRole) {
        super();
        this.idToken = idToken;
        this.accessToken = sessionToken;
        this.refreshToken = refreshToken;
        this.expiresIn = expiresIn;
        this.userData = userData;
        this.userRole = userRole;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getActualDate() {
        return actualDate;
    }

    public void setActualDate(String actualDate) {
        this.actualDate = actualDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public UserResponse getUserData() {
        return userData;
    }

    public void setUserData(UserResponse userData) {
        this.userData = userData;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getUserRole() { return userRole; }

    public void setUserRole(List<String> userRole) { this.userRole = userRole; }
}