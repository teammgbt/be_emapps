package com.microservice.authorization.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microservice.authorization.controller.ExceptionController;
import com.microservice.authorization.exception.CognitoException;
import com.microservice.authorization.security.model.ResponseWrapper;
import com.microservice.authorization.util.CorsHelper;
import com.nimbusds.jose.proc.BadJOSEException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by dendy-prtha on 02/04/2020.
 * AWS Cognito JWT Authentication Filter - Contains the logic to call the token validation with Cognito.
 *
 * @version 1.0
 *
 */
@Component
public class AwsCognitoJwtAuthenticationFilter extends OncePerRequestFilter {


	private static final String ERROR_OCCURED_WHILE_PROCESSING_THE_TOKEN = "Error occured while processing the token";
	private static final String INVALID_TOKEN_MESSAGE = "Invalid Token";

	private static final Logger classLogger = LoggerFactory.getLogger(AwsCognitoJwtAuthenticationFilter.class);

	private AwsCognitoTokenProcessor awsCognitoTokenProcessor;

	@Autowired
	private ApplicationContext appContext;

	public AwsCognitoJwtAuthenticationFilter(AwsCognitoTokenProcessor awsCognitoTokenProcessor) {
		this.awsCognitoTokenProcessor = awsCognitoTokenProcessor;
	}

	/**
	 * Creates an Exception Response
	 *@param response
	 *@param exception
	 *@throws IOException
	 *
	 */
	private void createExceptionResponse(ServletResponse response, CognitoException exception)
			throws IOException {
		ExceptionController exceptionController;
		ObjectMapper objMapper = new ObjectMapper();

		//ExceptionController is now accessible because I loaded it manually
		exceptionController = appContext.getBean(ExceptionController.class);
		//Calls the exceptionController
		ResponseWrapper responseWrapper = exceptionController.handleJwtException(exception);

		HttpServletResponse httpResponse  =  CorsHelper.addResponseHeaders(response);

		final HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(httpResponse);
		wrapper.setStatus(HttpStatus.UNAUTHORIZED.value());
		wrapper.setContentType(APPLICATION_JSON_VALUE);
		if (wrapper.getWriter() != null) {
			wrapper.getWriter().println(objMapper.writeValueAsString(responseWrapper));
			wrapper.getWriter().flush();
		}
	}

	/**
	 * do filter for every request
	* */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
			Authentication authentication;
			try {
				authentication = awsCognitoTokenProcessor.getIdAuthentication(request);
				SecurityContextHolder.getContext().setAuthentication(authentication);

			} catch (BadJOSEException e) {
				SecurityContextHolder.clearContext();
				classLogger.error(e.getMessage());
				createExceptionResponse(response, new CognitoException(INVALID_TOKEN_MESSAGE,
						CognitoException.INVALID_TOKEN_EXCEPTION_CODE, e.getMessage()));
				return;
			}catch (CognitoException e) {
				SecurityContextHolder.clearContext();
				classLogger.error(e.getMessage());
				createExceptionResponse(response, new CognitoException(e.getErrorMessage(),
						CognitoException.INVALID_TOKEN_EXCEPTION_CODE, e.getDetailErrorMessage()));
				return ;
			}catch(Exception e) {
				SecurityContextHolder.clearContext();
				classLogger.error(e.getMessage());
				createExceptionResponse(response, new CognitoException(ERROR_OCCURED_WHILE_PROCESSING_THE_TOKEN,
						CognitoException.INVALID_TOKEN_EXCEPTION_CODE, e.getMessage()));
				return ;
			}

		filterChain.doFilter(request,response);

	}
}