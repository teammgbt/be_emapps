/**
 * 
 */
package com.microservice.authorization.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dendy-prtha on 02/04/2020.
 * StringValidationHelper.java class that contains the logic
 * 
 * @version 
 * 
 */
public class StringValidationHelper {
	private static final Logger classLogger = LoggerFactory.getLogger(StringValidationHelper.class);

	public boolean validateEmailFormat(String email) {
		Pattern pattern = Pattern.compile("^(.+)@(.+)$"); //NOSONAR
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	/**
	 * validatePhoneNumber 
	 *@param phoneNumber
	 *@return isValid
	 * 
	 * 
	 */
	public boolean validatePhoneNumber(String phoneNumber) {
		Pattern pattern = Pattern.compile("(\\+62 ((\\d{3}([ -]\\d{3,})([- ]\\d{4,})?)|(\\d+)))|(\\(\\d+\\) \\d+)|\\d{3}( \\d+)+|(\\d+[ -]\\d+)|\\d+"); //NOSONAR
		Matcher matcher = pattern.matcher(phoneNumber);
		return matcher.matches();
	}

	/**
	 * hasOnlyAlphanumericCharacters - Validates if the text only has alphanumeric characters.
	 *@param text
	 *@return result - boolean
	 * 
	 * 
	 */
	public boolean hasOnlyAlphanumericCharacters(String text) {
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9- ]+$"); //NOSONAR
		Matcher matcher = pattern.matcher(text);
		return matcher.matches();
	}


	public Date convertStringToDate(String date, String format) throws ParseException {
		java.util.Date utilDate= new SimpleDateFormat(format).parse(date);
		return new Date(utilDate.getTime());
	}

	public String convertDateToString(java.util.Date date, String format) {
		String dateString = null;

		if(null != date) {
			DateFormat df = new SimpleDateFormat(format);
			dateString = df.format(date);
		}
		
		return dateString;
	}

	public boolean isValidDate(String date, String format) {
        boolean valid = false;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        	LocalDate.parse(date, formatter);
        	valid = true;

        } catch (Exception ignored) {
        	classLogger.error(ignored.getMessage());
        	throw ignored;
		}
        
        return valid;
    }
	
	public boolean isStateValid(String state) {
		Pattern pattern = Pattern
				.compile("AL|AK|AR|AZ|CA|CO|CT|DC|DE|FL|GA|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MI|MN|MO|MS|MT|NC|ND|NE|NH|NJ|NM|NV|NY|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VA|VT|WA|WI|WV|WY|al|ak|ar|az|ca|co|ct|dc|de|fl|ga|hi|ia|id|il|in|ks|ky|la|ma|md|me|mi|mn|mo|ms|mt|nc|nd|ne|nh|nj|nm|nv|ny|oh|ok|or|pa|ri|sc|sd|tn|tx|ut|va|vt|wa|wi|wv|wy"); //NOSONAR
		Matcher matcher = pattern.matcher(state);
		return matcher.matches();
	}

	public boolean hasSqlInjectionCharacters(String text) {
		Pattern pattern = Pattern.compile("^[^\\'\\{\\}\\\\\\;\\$]*$"); //NOSONAR
		Matcher matcher = pattern.matcher(text);
		return !matcher.matches();
	}
}
