package com.microservice.authorization.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.microservice.authorization.util.AWSConfig;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Created by dendy-prtha on 02/04/2020.
 * AWSClientProviderBuilder.java class that contains the logic
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class AWSClientProviderBuilder {

	@Autowired
	private  AWSConfig cognitoConfig;

	private AWSCognitoIdentityProvider cognitoIdentityProvider;
	private AmazonS3 s3Client;
	private ClasspathPropertiesFileCredentialsProvider propertiesFileCredentialsProvider;
	private String region;

	/**
	 * getAWSCognitoIdentityClient 
	 *
	 */
	private void initCommonInfo() {
		if(null == propertiesFileCredentialsProvider) {
			propertiesFileCredentialsProvider = new ClasspathPropertiesFileCredentialsProvider();
		}
		if(null == region) {
			region = cognitoConfig.getRegion();
		}
	}

	public AWSCognitoIdentityProvider getAWSCognitoIdentityClient() {
		if( null == cognitoIdentityProvider) {
			initCommonInfo();

			cognitoIdentityProvider = AWSCognitoIdentityProviderClientBuilder.standard()
					.withCredentials(propertiesFileCredentialsProvider)
					.withRegion(region)
					.build();
		}

		return cognitoIdentityProvider;
	}

	/** 
	 * Returns the Amazon S3 Client.
	 * @return s3Client - AmazonS3
	 */
	public AmazonS3 getAWSS3Client() {
		if( null == s3Client) {
			initCommonInfo();

			s3Client = AmazonS3ClientBuilder.standard()
					.withCredentials(propertiesFileCredentialsProvider)
					.withRegion(region)
					.build();

		}
		return s3Client;
	}

	/**
	 * Return SecretHash by UserName
	 * @param userName
	 * @return
	 */
	public String calculateSecretHash(String userName) throws NoSuchAlgorithmException, InvalidKeyException {

		initCommonInfo();
		String userPoolClientSecret = cognitoConfig.getClientSecret();
		String userPoolClientId = cognitoConfig.getClientId();

		final String HMAC_SHA256_ALGORITHM = "HmacSHA256";

		SecretKeySpec signingKey = new SecretKeySpec(
				userPoolClientSecret.getBytes(StandardCharsets.UTF_8),
				HMAC_SHA256_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(signingKey);
		mac.update(userName.getBytes(StandardCharsets.UTF_8));
		byte[] rawHmac = mac.doFinal(userPoolClientId.getBytes(StandardCharsets.UTF_8));
		return Base64.getEncoder().encodeToString(rawHmac);
	}
}
