package com.microservice.authorization.service;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.*;
import com.microservice.authorization.authentication.AWSClientProviderBuilder;
import com.microservice.authorization.exception.CognitoException;
import com.microservice.authorization.model.*;
import com.microservice.authorization.security.model.SpringSecurityUser;
import com.microservice.authorization.util.AWSConfig;
import com.microservice.authorization.util.Constants;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * AWSCognitoAuthService.java class that contains the logic to connect to Cognito
 */
@Service
public class AWSCognitoAuthService {
    /**
     * Password key
     */
    private static final String PASS_WORD = "PASSWORD";
    /**
     * Username key
     */
    private static final String USERNAME = "USERNAME";
    /**
     * SECRET_HASH key
     */
    private static final String SECRET_HASH = "SECRET_HASH";
    private static final String ADDRESS = "address";
    private static final String BIRTH_DATE = "birthdate";
    private static final String GENDER = "gender";
    private static final String MIDDLE_NAME ="middle_name";
    private static final String INVALID_USERNAME = "Invalid Username";
    private static final String SUCCESS = "SUCCESS";
    private static final String EMAIL = "email";
    private static final String NAME = "name";
    private static final String ORGANIZER = "ORGANIZER";

    private final Logger classLogger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AWSClientProviderBuilder cognitoBuilder;

    @Autowired
    private AWSConfig cognitoConfig;

    @Autowired(required = false)
    private AuthenticationManager authenticationManager;

    /**
     * getAmazonCognitoIdentityClient
     *
     * @return AWSCognitoIdentityProvider
     */
    private AWSCognitoIdentityProvider getAmazonCognitoIdentityClient() {
        return cognitoBuilder.getAWSCognitoIdentityClient();
    }

    /**
     * Method that contains the logic of authentication with AWS Cognito.
     *
     * @param authenticationRequest
     * @return SpringSecurityUser
     * (1.0)
     */
    public SpringSecurityUser authenticate(AuthenticationRequest authenticationRequest) {
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        try {
            final Map<String, String> authParams = new HashMap<>();
            authParams.put(USERNAME, authenticationRequest.getUsername());
            authParams.put(PASS_WORD, authenticationRequest.getPassword());
            authParams.put(SECRET_HASH, cognitoBuilder.calculateSecretHash(authenticationRequest.getUsername()));

            final InitiateAuthRequest authRequest = new InitiateAuthRequest()
                    .withAuthFlow(AuthFlowType.USER_PASSWORD_AUTH)
                    .withClientId(cognitoConfig.getClientId())
                    .withAuthParameters(authParams);

            InitiateAuthResult initiateAuthResult = cognitoClient.initiateAuth(authRequest);

            AuthenticationResultType authenticationResultType = initiateAuthResult.getAuthenticationResult();

            //AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN, ROLE_EMPLOYEE, ROLE_MANAGER")
            SpringSecurityUser userAuthenticated = new SpringSecurityUser(authenticationRequest.getUsername(), authenticationRequest.getPassword(), null, null, null);
            userAuthenticated.setAccessToken(authenticationResultType.getAccessToken());
            userAuthenticated.setExpiresIn(authenticationResultType.getExpiresIn());
            userAuthenticated.setTokenType(authenticationResultType.getTokenType());
            userAuthenticated.setRefreshToken(authenticationResultType.getRefreshToken());
            userAuthenticated.setIdToken(authenticationResultType.getIdToken());

            if (classLogger.isInfoEnabled()) {
                classLogger.info("User successfully authenticated userInfo: username {}", authenticationRequest.getUsername());
            }

            return userAuthenticated;
        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (CognitoException cognitoException) {
            throw cognitoException;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * SignUp - Method that contains the logic to Sign Up a specific user into Amazon Cognito.
     *
     * @param userSignUpRequest
     * @return UserSignUpResponse
     */
    public UserSignUpResponse signUp(UserSignUpRequest userSignUpRequest) {
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        if (classLogger.isInfoEnabled()) {
            classLogger.info("creating user {}", userSignUpRequest.getEmail());
        }
        try {
            String username = userSignUpRequest.getEmail();
            String userRole = userSignUpRequest.isOrganizer() ? ORGANIZER : cognitoConfig.getDeveloperGroup();

            SignUpRequest signUpRequest = new SignUpRequest()
                    .withClientId(cognitoConfig.getClientId())
                    .withUsername(username)
                    .withUserAttributes(new AttributeType().withName(ADDRESS).withValue(userSignUpRequest.getAddress()))
                    .withUserAttributes(new AttributeType().withName(BIRTH_DATE).withValue(userSignUpRequest.getBirthdate()))
                    .withUserAttributes(new AttributeType().withName(EMAIL).withValue(userSignUpRequest.getEmail()))
                    .withUserAttributes(new AttributeType().withName(GENDER).withValue(userSignUpRequest.getGender()))
                    .withUserAttributes(new AttributeType().withName(MIDDLE_NAME).withValue(userSignUpRequest.getMiddleName()))
                    .withUserAttributes(new AttributeType().withName(NAME).withValue(userSignUpRequest.getName()))
                    .withPassword(userSignUpRequest.getPassword())
                    .withSecretHash(cognitoBuilder.calculateSecretHash(username));

            SignUpResult signUpResult = cognitoClient.signUp(signUpRequest);

            //Adds user to group
            addUserToGroup(username, userRole);

            //get latest status
            AdminGetUserRequest userRequest = new AdminGetUserRequest().withUserPoolId(cognitoConfig.getPoolId()).withUsername(username);

            AdminGetUserResult cognitoUser = cognitoClient.adminGetUser(userRequest);
            UserSignUpResponse userResult = new UserSignUpResponse();
            userResult.setEmail(username);
            userResult.setEnabled(cognitoUser.getEnabled());
            userResult.setUserStatus(cognitoUser.getUserStatus());
            userResult.setLastModifiedDate(cognitoUser.getUserLastModifiedDate().toString());
            userResult.setUserCreatedDate(cognitoUser.getUserCreateDate().toString());
            userResult.setAwsResponse(signUpResult.toString());
            userResult.setUserRole(userRole);

            if (classLogger.isInfoEnabled()) {
                classLogger.info("signUpResult created {} ", signUpResult);
                classLogger.info("User created {} ", userResult);
            }
            return userResult;

        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (CognitoException cognitoException) {
            throw cognitoException;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * SignUpConfirm - Confirms the user sign up for a specific user in Amazon Cognito.
     *
     * @param userSignUpRequest
     */
    public UserSignUpResponse signUpConfirmation(UserSignUpConfirmationRequest userSignUpRequest) {
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        if (StringUtils.isBlank(userSignUpRequest.getEmail())) {
            throw new CognitoException("Invalid email", CognitoException.EMAIL_MISSING_EXCEPTION, "Invalid email");
        }

        if (classLogger.isInfoEnabled()) {
            classLogger.info("confirming signup of user {}", userSignUpRequest.getEmail());
        }

        try {

            String username = userSignUpRequest.getEmail();

            ConfirmSignUpRequest confirmSignUpRequest = new ConfirmSignUpRequest()
                    .withClientId(cognitoConfig.getClientId())
                    .withUsername(username)
                    .withConfirmationCode(userSignUpRequest.getConfirmationCode())
                    .withSecretHash(cognitoBuilder.calculateSecretHash(username));

            //Invokes the cognito authentication
            ConfirmSignUpResult confirmSignUpResult = cognitoClient.confirmSignUp(confirmSignUpRequest);

            if (classLogger.isInfoEnabled()) {
                classLogger.info("Sign up confirm successfully for user {} ", username);
            }

            //get latest status
            AdminGetUserRequest userRequest = new AdminGetUserRequest().withUserPoolId(cognitoConfig.getPoolId()).withUsername(username);

            AdminGetUserResult cognitoUser = cognitoClient.adminGetUser(userRequest);
            UserSignUpResponse userResult = new UserSignUpResponse();
            userResult.setEmail(username);
            userResult.setEnabled(cognitoUser.getEnabled());
            userResult.setUserStatus(cognitoUser.getUserStatus());
            userResult.setLastModifiedDate(cognitoUser.getUserLastModifiedDate().toString());
            userResult.setUserCreatedDate(cognitoUser.getUserCreateDate().toString());
            userResult.setAwsResponse(confirmSignUpResult.toString());

            //Get Access token from signUp
            AuthenticationRequest signinRequest = new AuthenticationRequest();
            signinRequest.setUsername(userSignUpRequest.getEmail());
            signinRequest.setPassword(userSignUpRequest.getPassword());
            AuthenticationResponse signinResponse = signIn(signinRequest);
            userResult.setAccessToken(signinResponse.getAccessToken());
            userResult.setIdToken(signinResponse.getIdToken());

            return userResult;
        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (CognitoException cognitoException) {
            throw cognitoException;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * resendConfirmationCode
     *
     * @param resendConfirmationCodeRequest
     */
    public UserSignUpResponse resendConfirmationCode(ResendConfCodeRequest resendConfirmationCodeRequest) {
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        if (StringUtils.isBlank(resendConfirmationCodeRequest.getUsername())) {
            throw new CognitoException(INVALID_USERNAME, CognitoException.EMAIL_MISSING_EXCEPTION, INVALID_USERNAME);
        }

        if (classLogger.isInfoEnabled()) {
            classLogger.info("resendConfirmationCode of user {}", resendConfirmationCodeRequest.getUsername());
        }

        try {

            String username = resendConfirmationCodeRequest.getUsername();

            ResendConfirmationCodeRequest resendCodeRequest = new ResendConfirmationCodeRequest()
                    .withClientId(cognitoConfig.getClientId())
                    .withUsername(username)
                    .withSecretHash(cognitoBuilder.calculateSecretHash(username));

            //Invokes the cognito authentication
            ResendConfirmationCodeResult resendConfirmationCodeResult = cognitoClient.resendConfirmationCode(resendCodeRequest);

            if (classLogger.isInfoEnabled()) {
                classLogger.info("Resend confirmation code successfully for user {} ", username);
            }

            UserSignUpResponse userResult = new UserSignUpResponse();
            userResult.setEmail(username);
            userResult.setAwsResponse(resendConfirmationCodeResult.toString());

            return userResult;
        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (CognitoException cognitoException) {
            throw cognitoException;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * addUserToGroup - Adds an specific user to an specific group
     *
     * @param username
     * @param groupname
     */
    public void addUserToGroup(String username, String groupname) {
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        if (classLogger.isInfoEnabled()) {
            classLogger.info(String.format("Adding user  %1$s, to %2$s group", username, groupname));
        }

        try {

            AdminAddUserToGroupRequest addUserToGroupRequest = new AdminAddUserToGroupRequest()
                    .withGroupName(groupname.toUpperCase())
                    .withUserPoolId(cognitoConfig.getPoolId())
                    .withUsername(username);

            cognitoClient.adminAddUserToGroup(addUserToGroupRequest);

            if (classLogger.isInfoEnabled()) {
                classLogger.info(String.format("User  %1$s added to %2$s group", username, groupname));
            }

        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * removeUserFromGroup - Remover an specific user from an specific group
     *
     * @param username
     * @param groupname
     */
    public void removeUserFromGroup(String username, String groupname) {
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        if (classLogger.isInfoEnabled()) {
            classLogger.info(String.format("Remove user  %1$s, from %2$s group", username, groupname));
        }

        try {

            AdminRemoveUserFromGroupRequest removeUserFromGroupRequest = new AdminRemoveUserFromGroupRequest()
                    .withGroupName(groupname.toUpperCase())
                    .withUserPoolId(cognitoConfig.getPoolId())
                    .withUsername(username);

            cognitoClient.adminRemoveUserFromGroup(removeUserFromGroupRequest);

            if (classLogger.isInfoEnabled()) {
                classLogger.info(String.format("User  %1$s removed from %2$s group", username, groupname));
            }

        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * signIn - do sign in mekanism
     *
     * @param authenticationRequest
     * @return authenticationResponse - object that contains all the sign in data.
     */
    public AuthenticationResponse signIn(AuthenticationRequest authenticationRequest) {

        String username = authenticationRequest.getUsername();
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        try
        {
            Map<String, String> authParams = new HashMap<>();
            authParams.put(USERNAME, username);
            authParams.put(PASS_WORD, authenticationRequest.getPassword());
            authParams.put(SECRET_HASH, cognitoBuilder.calculateSecretHash(username));

            InitiateAuthRequest initiateAuthRequest = new InitiateAuthRequest()
                    .withAuthFlow(AuthFlowType.USER_PASSWORD_AUTH)
                    .withClientId(cognitoConfig.getClientId())
                    .withAuthParameters(authParams);

            InitiateAuthResult initiateAuthResult = cognitoClient.initiateAuth(initiateAuthRequest);

            AuthenticationResultType authenticationResultType = initiateAuthResult.getAuthenticationResult();

            String idToken = authenticationResultType.getIdToken();
            String accessToken = authenticationResultType.getAccessToken();
            String refreshToken = authenticationResultType.getRefreshToken();
            String expiresIn = authenticationResultType.getExpiresIn().toString();
            List<String> userRole = getUserRole(username);

            UserResponse userResponse = getUserInfo(accessToken);

            return new AuthenticationResponse(idToken, accessToken, refreshToken, expiresIn, userResponse, userRole);
        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            String errorMessage = convertErrorMessageFromExceptionMessage(e.getMessage());
            throw new CognitoException(errorMessage, e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (CognitoException cognitoException) {
            throw cognitoException;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    public List<String> getUserRole(String username) {
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        List<String> userRole = new ArrayList<>();

        if (classLogger.isInfoEnabled()) {
            classLogger.info(String.format("Get user role for username %s", username));
        }

        try {
            AdminListGroupsForUserRequest adminListGroupsForUserRequest = new AdminListGroupsForUserRequest()
                    .withUserPoolId(cognitoConfig.getPoolId())
                    .withUsername(username);

            final AdminListGroupsForUserResult listGroupsForUser = cognitoClient.adminListGroupsForUser(adminListGroupsForUserRequest);

            for (GroupType role : listGroupsForUser.getGroups()) {
                userRole.add(role.getGroupName());
            }

            return userRole;
        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * SignOut - Signs out from an AWS Account
     *
     * @param accessToken
     * @param username
     * @return resultMessage
     */
    public String signOut(String accessToken, String username) {
        String resultMessage = null;
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        if (classLogger.isInfoEnabled()) {
            classLogger.info("User sign out request {}", username);
        }

        try {

            if (null != accessToken) {
                GlobalSignOutRequest globalSignOutRequest = new GlobalSignOutRequest()
                        .withAccessToken(accessToken);

                cognitoClient.globalSignOut(globalSignOutRequest);
                resultMessage = SUCCESS;

                if (classLogger.isInfoEnabled()) {
                    classLogger.info("User signed out {}", username);
                }

                return resultMessage;

            } else {
                throw new CognitoException("Missing access token", CognitoException.ACCESS_TOKEN_MISSING_EXCEPTION, "Missing access token");
            }


        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (CognitoException cognitoException) {
            throw cognitoException;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * getUserInfo - Returns the data of the specified user.
     *
     * @param accessToken
     * @return userResponse - object that contains all the cognito data.
     */
    public UserResponse getUserInfo(String accessToken) {
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        try {

            if (StringUtils.isBlank(accessToken)) {
                throw new CognitoException("User must provide an access token", CognitoException.INVALID_ACCESS_TOKEN_EXCEPTION, "User must provide an access token");
            }

            GetUserRequest userRequest = new GetUserRequest()
                    .withAccessToken(accessToken);

            GetUserResult userResult = cognitoClient.getUser(userRequest);

            List<AttributeType> userAttributes = userResult.getUserAttributes();
            return getUserAttributesData(userAttributes, userResult.getUsername());
        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (CognitoException cognitoException) {
            throw cognitoException;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }

    }

    /**
     * getUserAttributesData
     *
     * @param userAttributes
     * @param username
     * @return userResponse - Object filled with all the user data.
     */
    private UserResponse getUserAttributesData(List<AttributeType> userAttributes, String username) {
        UserResponse userResponse = new UserResponse();

        userResponse.setUsername(username);

        for (AttributeType attribute : userAttributes) {
            if (attribute.getName().equals(EMAIL)) {
                userResponse.setEmail(attribute.getValue());
            } else if (attribute.getName().equals(ADDRESS)) {
                userResponse.setAddress(attribute.getValue());
            } else if (attribute.getName().equals(NAME)) {
                userResponse.setName(attribute.getValue());
            } else if (attribute.getName().equals(MIDDLE_NAME)) {
                userResponse.setMiddleName(attribute.getValue());
            } else if (attribute.getName().equals(GENDER)) {
                userResponse.setGender(attribute.getValue());
            } else if (attribute.getName().equals(BIRTH_DATE)) {
                userResponse.setBirthDate(attribute.getValue());
            }
        }

        return userResponse;
    }

    /**
     * changePassword - Method that contains the logic to change password for a Amazon Cognito user.
     *
     * @param passwordRequest
     * @return
     */
    public PasswordResponse changePassword(PasswordRequest passwordRequest) {
        String username = passwordRequest.getUsername();
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        if (classLogger.isInfoEnabled()) {
            classLogger.info("change password {}", username);
        }

        try {
            //If username is blank it throws an error
            if (StringUtils.isBlank(username)) {
                throw new CognitoException(INVALID_USERNAME, CognitoException.INVALID_USERNAME_EXCEPTION, INVALID_USERNAME);
            }

            // ValidateCurrentPassword
            validateCurrentPassword(passwordRequest);

            ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest().
                    withAccessToken(passwordRequest.getAccessToken()).
                    withPreviousPassword(passwordRequest.getOldPassword()).
                    withProposedPassword(passwordRequest.getNewPassword());

            ChangePasswordResult changePasswordResult = cognitoClient.changePassword(changePasswordRequest);

            if (classLogger.isInfoEnabled()) {
                classLogger.info("Change password response Details: {} ", changePasswordResult);
            }

            PasswordResponse passwordResponse = new PasswordResponse();
            passwordResponse.setUsername(username);
            passwordResponse.setMessage(SUCCESS);
            return passwordResponse;

        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + " " + e.getErrorCode());
        } catch (CognitoException e) {
            classLogger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * Method for validate current password and username is correct
     *
     * @param passwordRequest
     * @return
     */
    private void validateCurrentPassword(PasswordRequest passwordRequest) {

        Map<String, String> credentials = new HashMap<>();
        credentials.put(Constants.PASS_WORD_KEY, passwordRequest.getOldPassword());

        try {
            AuthenticationRequest signInRequest = new AuthenticationRequest();
            signInRequest.setUsername(passwordRequest.getUsername());
            signInRequest.setPassword(passwordRequest.getOldPassword());

            //throws authenticationException if it fails !
            signIn(signInRequest);
        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            String errorMessage = convertErrorMessageFromExceptionMessage(e.getMessage());
            throw new CognitoException(errorMessage, e.getErrorCode(), e.getMessage() + " " + e.getErrorCode());
        } catch (CognitoException e) {
            classLogger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * forgotPassword - Method that contains the logic to send reset password code for a Amazon Cognito user.
     *
     * @param passwordRequest
     * @return PasswordResponse
     */
    public PasswordResponse forgotPassword(PasswordRequest passwordRequest) {
        String username = passwordRequest.getUsername();
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        if (classLogger.isInfoEnabled()) {
            classLogger.info("Reset password {}", username);
        }

        try {
            //If username is blank it throws an error
            if (StringUtils.isBlank(username)) {
                throw new CognitoException(INVALID_USERNAME, CognitoException.INVALID_USERNAME_EXCEPTION, INVALID_USERNAME);
            }

            String validateUsername = filterListUsersByUsername(username);
            if (StringUtils.isBlank(validateUsername)) {
                throw new CognitoException("Username is not registered", CognitoException.INVALID_USERNAME_EXCEPTION, INVALID_USERNAME);
            }

            ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest()
                    .withClientId(cognitoConfig.getClientId())
                    .withUsername(username)
                    .withSecretHash(cognitoBuilder.calculateSecretHash(username));

            ForgotPasswordResult forgotPasswordResult = cognitoClient.forgotPassword(forgotPasswordRequest);

            if (classLogger.isInfoEnabled()) {
                classLogger.info("Reset password response Delivery Details: {} ", forgotPasswordResult.getCodeDeliveryDetails());
            }

            PasswordResponse passwordResponse = new PasswordResponse();
            passwordResponse.setDestination(forgotPasswordResult.getCodeDeliveryDetails().getDestination());
            passwordResponse.setDeliveryMedium(forgotPasswordResult.getCodeDeliveryDetails().getDeliveryMedium());
            passwordResponse.setUsername(username);
            passwordResponse.setMessage(SUCCESS);
            return passwordResponse;

        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (CognitoException e) {
            classLogger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * confirmForgotPassword - Method that contains the logic to send reset password code for a Amazon Cognito user.
     *
     * @param passwordRequest
     * @return PasswordResponse
     */
    public PasswordResponse confirmForgotPassword(PasswordRequest passwordRequest) {
        String username = passwordRequest.getUsername();
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        if (classLogger.isInfoEnabled()) {
            classLogger.info("confirm Reset password {}", username);
        }

        try {
            //If username is blank it throws an error
            if (StringUtils.isBlank(username)) {
                throw new CognitoException(INVALID_USERNAME, CognitoException.INVALID_USERNAME_EXCEPTION, INVALID_USERNAME);
            }

            ConfirmForgotPasswordRequest confirmForgotPasswordRequest = new ConfirmForgotPasswordRequest()
                    .withClientId(cognitoConfig.getClientId())
                    .withUsername(username)
                    .withPassword(passwordRequest.getNewPassword())
                    .withConfirmationCode(passwordRequest.getConfirmationCode())
                    .withSecretHash(cognitoBuilder.calculateSecretHash(username));
            cognitoClient.confirmForgotPassword(confirmForgotPasswordRequest);

            if (classLogger.isInfoEnabled()) {
                classLogger.info("Reset password response Delivery Details: {} ", username);
            }

            PasswordResponse passwordResponse = new PasswordResponse();
            passwordResponse.setUsername(username);
            passwordResponse.setMessage(SUCCESS);
            return passwordResponse;

        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (CognitoException e) {
            classLogger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    /**
     * ConvertErrorMessageFromExceptionMessage
     *
     * @param exceptionErrorMessage
     * @return
     */
    private String convertErrorMessageFromExceptionMessage(String exceptionErrorMessage) {
        String result = exceptionErrorMessage;

        if (exceptionErrorMessage.contains("Incorrect username or password.")
                && exceptionErrorMessage.contains("NotAuthorizedException")) {
            result = "Incorrect username or password.";
        }

        return result;
    }

    /**
     * deleteUser - Deletes an specific User. USED only for the automated tests.
     *
     * @param user
     */
    public void deleteUser(String user) {
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        if (classLogger.isInfoEnabled()) {
            classLogger.info("Delete user {}", user);
        }

        try {

            AdminDeleteUserRequest deleteAdminUserRequest = new AdminDeleteUserRequest()
                    .withUserPoolId(cognitoConfig.getPoolId())
                    .withUsername(user);

            cognitoClient.adminDeleteUser(deleteAdminUserRequest);

            if (classLogger.isInfoEnabled()) {
                classLogger.info("User deleted {}", user);
            }

        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    private List<AttributeType> generateUserAttributesData(UpdateUserAttributeRequest request){

        List<AttributeType> userAttributes = new ArrayList<>();

        AttributeType userAttraddress = new AttributeType();
        userAttraddress.setName(ADDRESS);
        userAttraddress.setValue(request.getAddress());
        userAttributes.add(userAttraddress);

        AttributeType userAttrname = new AttributeType();
        userAttrname.setName(NAME);
        userAttrname.setValue(request.getName());
        userAttributes.add(userAttrname);

        AttributeType userAttrMiddleName = new AttributeType();
        userAttrMiddleName.setName(MIDDLE_NAME);
        userAttrMiddleName.setValue(request.getMiddleName());
        userAttributes.add(userAttrMiddleName);

        AttributeType userAttrgender = new AttributeType();
        userAttrgender.setName(GENDER);
        userAttrgender.setValue(request.getGender());
        userAttributes.add(userAttrgender);

        AttributeType userAttrbirthdate = new AttributeType();
        userAttrbirthdate.setName(BIRTH_DATE);
        userAttrbirthdate.setValue(request.getBirthdate());
        userAttributes.add(userAttrbirthdate);

        return userAttributes;
    }

    /**
     * updateUserAttributes - Update an specific User.
     *
     * @param updateRequest
     */
    public UpdateUserAttributeResponse updateUserAttributes(UpdateUserAttributeRequest updateRequest){
        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        if (classLogger.isInfoEnabled()) {
            classLogger.info("updateUserAttributes user {}", updateRequest.getUsername());
        }

        try{
            UpdateUserAttributesRequest request = new UpdateUserAttributesRequest();
            request.setAccessToken(updateRequest.getAccessToken());
            request.setUserAttributes(generateUserAttributesData(updateRequest));

            UpdateUserAttributesResult responseResult = cognitoClient.updateUserAttributes(request);

            UpdateUserAttributeResponse result = new UpdateUserAttributeResponse();
            result.setUsername(updateRequest.getUsername());
            result.setMessage(SUCCESS);
            result.setDeliveryDetailMessage(responseResult.toString());

            return result;
        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (CognitoException e) {
            classLogger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }

    public String filterListUsersByUsername(String username){

        AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

        try {
            String usernameFilter = "email = \"" + username + "\"";
            ListUsersRequest listUsersRequest = new ListUsersRequest()
                    .withUserPoolId(cognitoConfig.getPoolId())
                    .withFilter(usernameFilter)
                    .withLimit(5);
            ListUsersResult result = cognitoClient.listUsers(listUsersRequest);

            if(result.getUsers().isEmpty())
                return "";

            return result.getUsers().get(0).getUsername();
        } catch (com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), e.getErrorCode(), e.getMessage() + e.getErrorCode());
        } catch (Exception e) {
            classLogger.error(e.getMessage(), e);
            throw new CognitoException(e.getMessage(), CognitoException.GENERIC_EXCEPTION_CODE, e.getMessage());
        }
    }
}
