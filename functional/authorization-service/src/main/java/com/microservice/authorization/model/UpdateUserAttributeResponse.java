package com.microservice.authorization.model;

import lombok.Data;

@Data
public class UpdateUserAttributeResponse {

    private String message;
    private String username;
    private String deliveryDetailMessage;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDeliveryDetailMessage() {
        return deliveryDetailMessage;
    }

    public void setDeliveryDetailMessage(String deliveryDetailMessage) {
        this.deliveryDetailMessage = deliveryDetailMessage;
    }
}
