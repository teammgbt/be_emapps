package com.microservice.authorization.security.model;


import org.springframework.http.HttpStatus;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;


/**
 * Created by dendy-prtha on 02/04/2020.
 * Error list

 * 
 */
public class RestErrorList extends ArrayList<ErrorMessage> {


	/** Generated Serial Version*/
	private static final long serialVersionUID = -721424777198115589L;
	private HttpStatus status;

	public RestErrorList(HttpStatus status, ErrorMessage... errors) {
		this(status.value(), errors);
	}

	public RestErrorList(int status, ErrorMessage... errors) {
		super();
		this.status = HttpStatus.valueOf(status);
		addAll(asList(errors));
	}

	public RestErrorList(int status, List<ErrorMessage> errorMessages) {
		super();
		this.status = HttpStatus.valueOf(status);
		addAll(errorMessages);
	}

	/**
	 * @return the status
	 */
	public HttpStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(HttpStatus status) {
		this.status = status;
	}

}