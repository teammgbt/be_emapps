/**
 * 
 */
package com.microservice.authorization.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by dendy-prtha on 02/04/2020.
 * 
 * @version
 * 
 */

@Configuration
@PropertySource("classpath:aws.properties")
@ConfigurationProperties
@Data
public class AWSConfig {

	private static final String COGNITO_IDENTITY_POOL_URL = "https://cognito-idp.%s.amazonaws.com/%s";
	private static final String JSON_WEB_TOKEN_SET_URL_SUFFIX = "/.well-known/jwks.json";

	private String clientId;
	private String clientSecret;
	private String poolId;
	private String endpoint;
	private String region;
	private String identityPoolId;
	private String developerGroup;
	
	public static final String USERNAME = "cognito:username";
	public static final String GROUP_FIELD = "cognito:groups";
	public static final int CONNECTION_TIMEOUT = 2000;
	public static final int READ_TIMEOUT = 2000;
	public static final String HTTP_HEADER = "Authorization";

	public String getJwkUrl() {
		String cognitoURL = COGNITO_IDENTITY_POOL_URL +
				JSON_WEB_TOKEN_SET_URL_SUFFIX;
		return String.format(cognitoURL,region,poolId);
	}

	public String getCognitoIdentityPoolUrl() {
		return String.format(COGNITO_IDENTITY_POOL_URL,region,poolId);
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getPoolId() {
		return poolId;
	}

	public void setPoolId(String poolId) {
		this.poolId = poolId;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getIdentityPoolId() {
		return identityPoolId;
	}

	public void setIdentityPoolId(String identityPoolId) {
		this.identityPoolId = identityPoolId;
	}

	public String getDeveloperGroup() {
		return developerGroup;
	}

	public void setDeveloperGroup(String developerGroup) {
		this.developerGroup = developerGroup;
	}
}
