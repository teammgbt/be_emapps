package com.microservice.authorization.model;

import lombok.Data;

/**
 * Created by dendy-prtha on 02/04/2020.
 *
 * UserRequest.java class that contains the logic
 * 
 */
@Data
public class UserRequest {
	
	private String accessToken;
	private String username;
	private String userRole;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserRole() { return userRole; }

	public void setUserRole(String userRole) { this.userRole = userRole; }
}
