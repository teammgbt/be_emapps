package com.microservice.authorization.model;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserSignUpConfirmationRequest {

    @NotNull(message = "Email should not be Null")
    @NotEmpty(message = "Email should not be empty")
    @Email(message = "Email Should be valid")
    private String email;

    @NotNull(message = "Email should not be Null")
    @NotEmpty(message = "Email should not be empty")
    private String confirmationCode;

    @Size(min = 6, max = 20, message
            = "Password must be between 6 and 20 characters")
    private String password;
}
