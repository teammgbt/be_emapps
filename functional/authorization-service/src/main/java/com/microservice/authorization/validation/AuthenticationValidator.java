package com.microservice.authorization.validation;

import com.microservice.authorization.model.UpdateUserAttributeRequest;
import com.microservice.authorization.model.UserSignUpRequest;
import com.microservice.authorization.security.model.ErrorMessage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ryan_w737 on July 24, 2020
 */
@Service
public class AuthenticationValidator {
    private static final String BIRTH_DATE = "BirthDate";
    private static final String DATE_FORMAT = "dd-MM-yyyy";
    private final SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);

    public List<ErrorMessage> validate(UserSignUpRequest request) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (StringUtils.isBlank(request.getAddress())) {
            errorMessages.add(new ErrorMessage("Address", "Address must be set"));
        }
        if (StringUtils.isBlank(request.getBirthdate())) {
            errorMessages.add(new ErrorMessage(BIRTH_DATE, "BirthDate must be set"));
        } else {
            try {
                formatter.setLenient(false);
                Date date = formatter.parse(request.getBirthdate());
                formatter.applyPattern(DATE_FORMAT);
                request.setBirthdate(formatter.format(date));
            } catch (ParseException e) {
                if (e.getErrorOffset() == 10) {
                    errorMessages.add(new ErrorMessage(BIRTH_DATE, "Invalid date"));
                } else {
                    errorMessages.add(new ErrorMessage(BIRTH_DATE, "Invalid format birth date"));
                }
            }
        }
        if (StringUtils.isBlank(request.getEmail())) {
            errorMessages.add(new ErrorMessage("Email", "Email must be set"));
        } else {
            Pattern pattern = Pattern.compile("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$");
            Matcher matcher = pattern.matcher(request.getEmail());
            if (!matcher.matches()) {
                errorMessages.add(new ErrorMessage("Email", "Invalid format email"));
            }
        }
        if (StringUtils.isBlank(request.getGender())) {
            errorMessages.add(new ErrorMessage("Gender", "Gender must be set"));
        }
        if (StringUtils.isBlank(request.getPassword())) {
            errorMessages.add(new ErrorMessage("Password", "Password must be set"));
        } else if (request.getPassword().length() < 6 || request.getPassword().length() > 20 ) {
            errorMessages.add(new ErrorMessage("Password",
                    "Password must be between 6 and 20 characters"));
        }
        if (StringUtils.isBlank(request.getMiddleName())) {
            errorMessages.add(new ErrorMessage("MiddleName", "Middle name must be set"));
        }
        if (StringUtils.isBlank(request.getName())) {
            errorMessages.add(new ErrorMessage("Name", "Name must be set"));
        }
        return errorMessages;
    }

    public List<ErrorMessage> validateUser(UpdateUserAttributeRequest request) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (StringUtils.isNotBlank(request.getBirthdate())) {
            try {
                formatter.setLenient(false);
                Date date = formatter.parse(request.getBirthdate());
                formatter.applyPattern(DATE_FORMAT);
                request.setBirthdate(formatter.format(date));
            } catch (ParseException e) {
                if (e.getErrorOffset() == 10) {
                    errorMessages.add(new ErrorMessage(BIRTH_DATE, "Invalid date"));
                } else {
                    errorMessages.add(new ErrorMessage(BIRTH_DATE, "Invalid format birth date"));
                }
            }
        }
        return errorMessages;
    }
}
