package com.microservice.authorization.controller;

import com.microservice.authorization.model.*;
import com.microservice.authorization.security.model.ErrorMessage;
import com.microservice.authorization.security.model.ResponseWrapper;
import com.microservice.authorization.service.AWSCognitoAuthService;
import com.microservice.authorization.validation.AuthenticationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static java.util.Collections.singletonMap;

/**
 * Created by dendy-prtha on 02/04/2020.
 * SpringBoot Authentication Controller
 */

@RestController
@RequestMapping("auth")
public class AuthenticationController {

    private static final String STATUS_KEY = "status";

    @Autowired(required = false)
    private AWSCognitoAuthService awsAuthService;

    @Autowired
    private AuthenticationValidator validation;
    /**
     * Spring Controller that has the logic to authenticate.
     *
     * @param authenticationRequest
     * @return ResponseEntity<?>
     */
    @SuppressWarnings("unchecked")
    @PostMapping(value = "/SignIn")
    public ResponseEntity<ResponseWrapper> authenticationRequest(@RequestBody AuthenticationRequest authenticationRequest) {
        ResponseWrapper responseWrapper = new ResponseWrapper(
                awsAuthService.signIn(authenticationRequest),
                singletonMap(STATUS_KEY, HttpStatus.OK), null);

        // Return the response
        return ResponseEntity.ok(createOkResponse(responseWrapper));
    }

    private ResponseWrapper createOkResponse(Object object) {
        return new ResponseWrapper(
                object,
                singletonMap(STATUS_KEY, HttpStatus.OK), null);
    }

    /**
     * SignUpRequest - Method that signs up a user into Amazon Cognito
     *
     * @param signUpRequest
     * @return ResponseEntity<User>
     */
    @PostMapping(value = "/SignUp")
    public ResponseEntity<ResponseWrapper> signUpRequest(@Valid @RequestBody UserSignUpRequest signUpRequest) {
        //Calls the service that Signs up an specific User
        List<ErrorMessage> errorMessages = validation.validate(signUpRequest);
        if (!errorMessages.isEmpty()) {
            return new ResponseEntity(createFailedResponse(errorMessages), HttpStatus.NOT_ACCEPTABLE);
        }
        UserSignUpResponse response = awsAuthService.signUp(signUpRequest);

        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    /**
     * signUpConfirmation - Method that confirmation signs up a user into Amazon Cognito
     * @param signUpRequest
     * @return
     */
    @PostMapping(value = "/SignUpConfirmation")
    public ResponseEntity<ResponseWrapper> signUpConfirmation(@Valid @RequestBody UserSignUpConfirmationRequest signUpRequest) {
        //Calls the service that Signs up an specific User
        UserSignUpResponse response = awsAuthService.signUpConfirmation(signUpRequest);

        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    /**
     * resendConfirmationCode - Method that resend confirmation code
     * @param resendConfirmationCodeRequest
     * @return
     */
    @PostMapping(value = "/ResendConfirmationCode")
    public ResponseEntity<ResponseWrapper> resendConfirmationCode(@Valid @RequestBody ResendConfCodeRequest resendConfirmationCodeRequest) {
        //Calls the service that Signs up an specific User
        UserSignUpResponse response = awsAuthService.resendConfirmationCode(resendConfirmationCodeRequest);

        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    /**
     * resetPassword
     *
     * @param resetPasswordRequest
     * @return ResponseEntity<PasswordResponse>
     */
    @PostMapping(value = "/ForgotPassword")
    public ResponseEntity<ResponseWrapper> forgotPassword(@Valid @RequestBody PasswordRequest resetPasswordRequest) {
        //Calls the service that Signs up an specific User
        PasswordResponse response = awsAuthService.forgotPassword(resetPasswordRequest);

        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    /**
     * confirmResetPassword
     *
     * @param resetPasswordRequest
     * @return ResponseEntity<User>
     */
    @PostMapping(value = "/ConfirmForgotPassword")
    public ResponseEntity<ResponseWrapper> confirmForgotPassword(@RequestBody PasswordRequest resetPasswordRequest) {

        //Calls the service that Signs up an specific User
        PasswordResponse response = awsAuthService.confirmForgotPassword(resetPasswordRequest);
        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    /**
     * SignUpRequest - Method that signs up a user into Amazon Cognito
     *
     * @param authenticationRequest
     * @return ResponseEntity<User>
     */
    @PostMapping(value = "/SignOut")
    public ResponseEntity<ResponseWrapper> signOut(@RequestBody AuthenticationRequest authenticationRequest) {
        AuthenticationResponse response = new AuthenticationResponse();

        //Calls the service that Signs up an specific User
        String message = awsAuthService.signOut(authenticationRequest.getAccessToken(), authenticationRequest.getUsername());
        response.setMessage(message);
        response.setUsername(authenticationRequest.getUsername());
        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    /**
     * deleteUser
     *
     * @param userRequest
     * @return ResponseEntity<ResponseWrapper>
     */
    @DeleteMapping(value = "/DeleteUser")
    public ResponseEntity<ResponseWrapper> deleteUser(@RequestBody UserRequest userRequest) {
        AuthenticationResponse response = new AuthenticationResponse();

        awsAuthService.deleteUser(userRequest.getUsername());

        response.setMessage("User Deleted");
        response.setUsername(userRequest.getUsername());
        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    /**
     * changePassword
     *
     * @param resetPasswordRequest
     * @return ResponseEntity<ResponseWrapper>
     */
    @PostMapping(value = "/ChangePassword")
    public ResponseEntity<ResponseWrapper> changePassword(@RequestBody PasswordRequest resetPasswordRequest) {

        //Calls the service that Signs up an specific User
        PasswordResponse response = awsAuthService.changePassword(resetPasswordRequest);
        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    /**
     * getUserByToken
     *
     * @param authenticationRequest
     * @return ResponseEntity<ResponseWrapper>
     */
    @PostMapping(value = "/GetUserByToken")
    public ResponseEntity<ResponseWrapper> getUserByToken(@RequestBody AuthenticationRequest authenticationRequest) {

        //Calls the service that Signs up an specific User
        UserResponse response = awsAuthService.getUserInfo(authenticationRequest.getAccessToken());

        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    @PostMapping(value = "/UpdateUser")
    public ResponseEntity<ResponseWrapper> updateUser(@RequestBody UpdateUserAttributeRequest updateRequest) {

        List<ErrorMessage> errorMessages = validation.validateUser(updateRequest);
        if (!errorMessages.isEmpty()) {
            return new ResponseEntity(createFailedResponse(errorMessages), HttpStatus.NOT_ACCEPTABLE);
        }
        //Calls the service
        UpdateUserAttributeResponse response = awsAuthService.updateUserAttributes(updateRequest);

        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    /**
     * Test API is up
     * @return
     */
    @GetMapping(value = "/test")
    public ResponseEntity<ResponseWrapper> testAPI(@RequestParam(required = false) String str){
        UserResponse response = new UserResponse();
        String msg = "test user response ";
        String output = str != null && !str.isEmpty() ? msg+str : msg;
        response.setName(output);

        // Return the response
        return ResponseEntity.ok(createOkResponse(response));
    }

    private ResponseWrapper createFailedResponse(List<ErrorMessage> errorMessages) {
        return new ResponseWrapper(null, singletonMap(STATUS_KEY, HttpStatus.NOT_ACCEPTABLE), errorMessages);
    }
}
