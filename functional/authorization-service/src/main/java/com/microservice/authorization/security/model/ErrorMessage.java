package com.microservice.authorization.security.model;

import lombok.Data;

/**
 * Created by dendy-prtha on 02/04/2020.
 * Error message class
 * 
 */
@Data
public class ErrorMessage {
	

	private String message;
	private String code;
    private String detail;

	/**
	 * @param message
	 * @param code
	 * @param detail
	 */
	public ErrorMessage(String message, String code, String detail) {
		super();
		this.message = message;
		this.code = code;
		this.detail = detail;
	}

	public ErrorMessage(String message, String code) {
		super();
		this.message = message;
		this.code = code;
		this.detail = null;
	}

	public ErrorMessage(String message) {
		super();
		this.message = message;
		this.code = null;
		this.detail = null;
	}

	public ErrorMessage() {
		super();
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
}
