package com.microservice.authorization.controller;

import com.microservice.authorization.model.*;
import com.microservice.authorization.security.model.ResponseWrapper;
import com.microservice.authorization.service.AWSCognitoAuthService;
import com.microservice.authorization.validation.AuthenticationValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import static java.util.Collections.singletonMap;
import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationControllerTests {

    @InjectMocks
    AuthenticationController authenticationController;

    @Mock
    AWSCognitoAuthService authService;

    @Mock
    AuthenticationValidator validator;

    private static final String USER_NAME = "userName1@mail.com";
    private static final String PASSWORD = "password";              //NOSONAR
    private static final String SUCCESS_MESSAGE = "SUCCESS";
    private static final String ACCESS_TOKEN = "token1";
    private static final String STATUS = "status";
    private static final String DUMMY_GUID = "dummy";

    @Test
    public void testApiReturnSuccessMessage() {
        //test
        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.testAPI("");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        final ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());
        assertNull(responseWrapper.getErrors());

        final UserResponse response = (UserResponse) responseWrapper.getData();
        assertEquals("test user response ", response.getName());
    }

    @Test
    public void signOutReturnSuccessMessage(){
        AuthenticationRequest request = new AuthenticationRequest();
        request.setUsername(USER_NAME);
        request.setAccessToken(ACCESS_TOKEN);

        //mock
        when(authService.signOut(ACCESS_TOKEN, USER_NAME))
                .thenReturn(SUCCESS_MESSAGE);

        //test
        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.signOut(request);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(Objects.requireNonNull(responseEntity.getBody()).getData());

        AuthenticationResponse authResponse = (AuthenticationResponse) responseEntity.getBody().getData();

        assertEquals(SUCCESS_MESSAGE, authResponse.getMessage());
        assertEquals(USER_NAME, authResponse.getUsername());
    }

    @Test
    public void signUpRequestReturnSuccess() {
        UserSignUpRequest authRequest = new UserSignUpRequest();
        authRequest.setEmail(USER_NAME);
        authRequest.setPassword(PASSWORD);

        UserSignUpResponse signUpResponse = new UserSignUpResponse();
        signUpResponse.setUserStatus(STATUS);
        signUpResponse.setEmail(USER_NAME);
        signUpResponse.setAccessToken(ACCESS_TOKEN);
        signUpResponse.setEnabled(Boolean.TRUE);
        signUpResponse.setUserStatus(DUMMY_GUID);
        signUpResponse.setLastModifiedDate(DUMMY_GUID);
        signUpResponse.setUserCreatedDate(DUMMY_GUID);
        signUpResponse.setAwsResponse(DUMMY_GUID);

        doReturn(signUpResponse).when(authService).signUp(authRequest);
        when(validator.validate(authRequest)).thenReturn(new ArrayList<>());

        //Test
        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.signUpRequest(authRequest);

        //Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());

        UserSignUpResponse authenticationResponse1 = (UserSignUpResponse) responseWrapper.getData();
        assertEquals(USER_NAME, authenticationResponse1.getEmail());
    }


    @Test
    public void signUpConfirmationReturnSuccess() {
        UserSignUpConfirmationRequest authRequest = new UserSignUpConfirmationRequest();
        authRequest.setEmail(USER_NAME);
        authRequest.setConfirmationCode(PASSWORD);

        UserSignUpResponse signUpResponse = new UserSignUpResponse();
        signUpResponse.setUserStatus(STATUS);
        signUpResponse.setEmail(USER_NAME);
        signUpResponse.setAccessToken(ACCESS_TOKEN);
        signUpResponse.setEnabled(true);
        signUpResponse.setUserStatus(DUMMY_GUID);
        signUpResponse.setLastModifiedDate(DUMMY_GUID);
        signUpResponse.setUserCreatedDate(DUMMY_GUID);
        signUpResponse.setAwsResponse(DUMMY_GUID);

        doReturn(signUpResponse).when(authService).signUpConfirmation(authRequest);

        //Test
        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.signUpConfirmation(authRequest);

        //Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());

        UserSignUpResponse authenticationResponse1 = (UserSignUpResponse) responseWrapper.getData();
        assertEquals(USER_NAME, authenticationResponse1.getEmail());
    }


    @Test
    public void authenticationRequestReturnSuccessMessage() {
        Date date = new Date();
        AuthenticationRequest authRequest = new AuthenticationRequest();
        authRequest.setUsername(USER_NAME);
        authRequest.setPassword(PASSWORD);

        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setUsername(USER_NAME);
        authenticationResponse.setAccessToken(ACCESS_TOKEN);
        authenticationResponse.setIdToken(ACCESS_TOKEN);
        authenticationResponse.setRefreshToken(ACCESS_TOKEN);
        authenticationResponse.setExpiresIn(DUMMY_GUID);
        authenticationResponse.setActualDate(date.toString());
        authenticationResponse.setExpirationDate(date.toString());

        when(authService.signIn(authRequest)).
                thenReturn(authenticationResponse);

        //Test
        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.authenticationRequest(authRequest);

        //Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());

        ResponseWrapper returnResponse = (ResponseWrapper) responseWrapper.getData();
        AuthenticationResponse authenticationResponse1 = (AuthenticationResponse) returnResponse.getData();
        assertEquals(USER_NAME, authenticationResponse1.getUsername());
        assertEquals(ACCESS_TOKEN, authenticationResponse1.getAccessToken());
        assertEquals(ACCESS_TOKEN, authenticationResponse1.getIdToken());
        assertEquals(ACCESS_TOKEN, authenticationResponse1.getRefreshToken());
        assertEquals(DUMMY_GUID, authenticationResponse1.getExpiresIn());
        assertEquals(date.toString(), authenticationResponse1.getActualDate());
        assertEquals(date.toString(), authenticationResponse1.getExpirationDate());
    }

    @Test
    public void changePasswordReturnSuccess() {
        String testOldPassword = "oldPassword";
        String testNewPassword = "NewPassword";
        PasswordRequest passwordRequest = new PasswordRequest();
        passwordRequest.setUsername(USER_NAME);
        passwordRequest.setOldPassword(testOldPassword);
        passwordRequest.setNewPassword(testNewPassword);

        PasswordResponse passwordResponse = new PasswordResponse();
        passwordResponse.setUsername(USER_NAME);

        //Mock
        when(authService.changePassword(Mockito.any()))
                .thenReturn(passwordResponse);

        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.changePassword(passwordRequest);

        //Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        responseWrapper.setErrors(new ArrayList<>());
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());

        PasswordResponse returnResponse = (PasswordResponse) responseWrapper.getData();
        assertEquals(USER_NAME, returnResponse.getUsername());
        assertEquals(singletonMap(STATUS, HttpStatus.OK), responseWrapper.getMetadata());
        assertEquals(0, responseWrapper.getErrors().size());
    }

    @Test
    public void confirmResetPasswordReturnSuccess() {
        PasswordRequest passwordRequest = new PasswordRequest();
        passwordRequest.setUsername(USER_NAME);
        PasswordResponse passwordResponse = new PasswordResponse();
        passwordResponse.setUsername(USER_NAME);

        //Mock
        when(authService.confirmForgotPassword(Mockito.any()))
                .thenReturn(passwordResponse);

        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.confirmForgotPassword(passwordRequest);

        //Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());

        PasswordResponse returnResponse = (PasswordResponse) responseWrapper.getData();
        assertEquals(USER_NAME, returnResponse.getUsername());
        assertEquals(singletonMap(STATUS, HttpStatus.OK), responseWrapper.getMetadata());
        assertNull(responseWrapper.getErrors());
    }

    @Test
    public void resetPasswordReturnSuccess() {
        PasswordRequest passwordRequest = new PasswordRequest();
        passwordRequest.setUsername(USER_NAME);
        PasswordResponse passwordResponse = new PasswordResponse();
        passwordResponse.setUsername(USER_NAME);

        //Mock
        when(authService.forgotPassword(Mockito.any()))
                .thenReturn(passwordResponse);

        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.forgotPassword(passwordRequest);

        //Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());

        PasswordResponse returnResponse = (PasswordResponse) responseWrapper.getData();
        assertEquals(USER_NAME, returnResponse.getUsername());
        assertEquals(singletonMap(STATUS, HttpStatus.OK), responseWrapper.getMetadata());
        assertNull(responseWrapper.getErrors());
    }

    @Test
    public void getUserByTokenReturnSuccessMessage() {
        String date = "2020-01-01";
        AuthenticationRequest authRequest = new AuthenticationRequest();
        authRequest.setAccessToken(ACCESS_TOKEN);

        UserResponse authenticationResponse = new UserResponse();
        authenticationResponse.setUsername(USER_NAME);
        authenticationResponse.setBrokerID(DUMMY_GUID);
        authenticationResponse.setUserCreateDate(date);
        authenticationResponse.setUserStatus(STATUS);
        authenticationResponse.setLastModifiedDate(date);

        doReturn(authenticationResponse).when(authService).getUserInfo(ACCESS_TOKEN);

        //Test
        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.getUserByToken(authRequest);

        //Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());

        UserResponse authenticationResponse1 = (UserResponse) responseWrapper.getData();
        assertEquals(USER_NAME, authenticationResponse1.getUsername());
        assertEquals(DUMMY_GUID, authenticationResponse1.getBrokerID());
        assertEquals(date, authenticationResponse1.getUserCreateDate());
        assertEquals(STATUS, authenticationResponse1.getUserStatus());
        assertEquals(date, authenticationResponse1.getLastModifiedDate());
    }

    @Test
    public void updateUserReturnSuccessMessage() {
        UpdateUserAttributeRequest authRequest = new UpdateUserAttributeRequest();
        authRequest.setAccessToken(ACCESS_TOKEN);
        authRequest.setUsername(USER_NAME);
        authRequest.setAddress("Address 123123 No 547855");
        authRequest.setBirthdate("04-Jan-1986");
        authRequest.setGender("male");
        authRequest.setMiddleName("_testMiddleName");
        authRequest.setName("_testName");

        UpdateUserAttributeResponse authenticationResponse = new UpdateUserAttributeResponse();
        authenticationResponse.setUsername(USER_NAME);

        doReturn(authenticationResponse).when(authService).updateUserAttributes(authRequest);
        when(validator.validateUser(authRequest)).thenReturn(new ArrayList<>());

        //Test
        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.updateUser(authRequest);

        //Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());

        UpdateUserAttributeResponse authenticationResponse1 = (UpdateUserAttributeResponse) responseWrapper.getData();
        assertEquals(USER_NAME, authenticationResponse1.getUsername());
    }

    @Test
    public void resendConfirmationCodeReturnSuccessMessage() {
        when(authService.resendConfirmationCode(new ResendConfCodeRequest())).thenReturn(new UserSignUpResponse());
        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.resendConfirmationCode(new ResendConfCodeRequest());

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());
        assertNull(responseWrapper.getErrors());
    }

    @Test
    public void deleteUserReturnSuccessMessage() {
        final UserRequest request = new UserRequest();
        request.setUsername(USER_NAME);
        ResponseEntity<ResponseWrapper> responseEntity = authenticationController.deleteUser(request);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());
        assertNull(responseWrapper.getErrors());

        final AuthenticationResponse authResponse = (AuthenticationResponse) responseWrapper.getData();
        assertEquals(USER_NAME, authResponse.getUsername());
        assertEquals("User Deleted", authResponse.getMessage());
    }
}
