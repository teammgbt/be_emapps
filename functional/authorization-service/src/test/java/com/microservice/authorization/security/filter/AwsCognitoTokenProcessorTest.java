package com.microservice.authorization.security.filter;

import com.microservice.authorization.exception.CognitoException;
import com.microservice.authorization.model.UserResponse;
import com.microservice.authorization.security.config.CognitoJwtAuthentication;
import com.microservice.authorization.service.AWSCognitoAuthService;
import com.microservice.authorization.util.AWSConfig;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class AwsCognitoTokenProcessorTest {
    private static final String POOL_URL = "https://cognito-idp.%s.amazonaws.com/%s";
    private static final String ID_TOKEN = "Bearer idtoken123";
    private static final String USERNAME = "user";
    private static final String INVALID_TOKEN = "Invalid Token";

    @InjectMocks
    private AwsCognitoTokenProcessor processor;

    @Mock
    private AWSConfig jwtConfiguration;

    @Mock
    private ConfigurableJWTProcessor configurableJWTProcessor;

    @Mock
    private HttpServletRequest request;

    @Mock
    private AWSCognitoAuthService service;

    @Test
    public void getIdAuthenticationReturnSuccess() throws Exception {
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .issuer(POOL_URL)
                .claim(AWSConfig.USERNAME, USERNAME)
                .claim(AWSConfig.GROUP_FIELD, Collections.singletonList("ADMIN"))
                .claim("token_use", "id")
                .build();
        when(request.getHeader(AWSConfig.HTTP_HEADER)).thenReturn(ID_TOKEN);
        when(jwtConfiguration.getCognitoIdentityPoolUrl()).thenReturn(POOL_URL);
        when(configurableJWTProcessor.process(anyString(), any())).thenReturn(claimsSet);
        Authentication authentication = processor.getIdAuthentication(request);
        assertEquals(((CognitoJwtAuthentication) authentication).getJwtClaimsSet(), claimsSet);
    }

    @Test
    public void getIdAuthenticationWithInvalidIssuerThrowsCognitoException() throws Exception {
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .issuer(POOL_URL)
                .claim(AWSConfig.USERNAME, USERNAME)
                .claim(AWSConfig.GROUP_FIELD, Collections.singletonList("ADMIN"))
                .claim("token_use", "id")
                .build();
        when(request.getHeader(AWSConfig.HTTP_HEADER)).thenReturn(ID_TOKEN);
        when(jwtConfiguration.getCognitoIdentityPoolUrl()).thenReturn("random_issuer");
        when(configurableJWTProcessor.process(anyString(), any())).thenReturn(claimsSet);
        try {
            processor.getIdAuthentication(request);
        } catch (CognitoException e){
            assertInvalidIssuerException(e);
        }
    }

    @Test
    public void getIdAuthenticationWithInvalidIdTokenThrowsCognitoException() throws Exception {
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .issuer(POOL_URL)
                .claim(AWSConfig.USERNAME, USERNAME)
                .claim(AWSConfig.GROUP_FIELD, Collections.singletonList("ADMIN"))
                .claim("token_use", "random_id")
                .build();
        when(request.getHeader(AWSConfig.HTTP_HEADER)).thenReturn(ID_TOKEN);
        when(jwtConfiguration.getCognitoIdentityPoolUrl()).thenReturn(POOL_URL);
        when(configurableJWTProcessor.process(anyString(), any())).thenReturn(claimsSet);
        try {
            processor.getIdAuthentication(request);
        } catch (CognitoException e){
            assertNotTokenException(e, "ID");
        }
    }

    @Test
    public void getSessionAuthenticationReturnSuccess() throws Exception {
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .issuer(POOL_URL)
                .claim(AWSConfig.USERNAME, USERNAME)
                .claim(AWSConfig.GROUP_FIELD, Collections.singletonList("ADMIN"))
                .claim("token_use", "access")
                .build();
        final UserResponse response = new UserResponse();
        response.setUsername(USERNAME);
        when(request.getHeader(AWSConfig.HTTP_HEADER)).thenReturn(ID_TOKEN);
        when(jwtConfiguration.getCognitoIdentityPoolUrl()).thenReturn(POOL_URL);
        when(configurableJWTProcessor.process(anyString(), any())).thenReturn(claimsSet);
        when(service.getUserInfo(anyString())).thenReturn(response);
        Authentication authentication = processor.getSessionAuthentication(request);
        assertEquals(((CognitoJwtAuthentication) authentication).getJwtClaimsSet(), claimsSet);
    }

    @Test
    public void getSessionAuthenticationWithoutIdTokenThrowsCognitoException() throws Exception {
        try {
            when(request.getHeader(AWSConfig.HTTP_HEADER)).thenReturn(null);
            processor.getSessionAuthentication(request);
        } catch (CognitoException e) {
            asssertTokenNotFoundException(e);
        }
    }

    @Test
    public void getSessionAuthenticationWithInvalidIssuerThrowsCognitoException() throws Exception {
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .issuer(POOL_URL)
                .claim(AWSConfig.USERNAME, USERNAME)
                .claim(AWSConfig.GROUP_FIELD, Collections.singletonList("ADMIN"))
                .claim("token_use", "id")
                .build();
        when(request.getHeader(AWSConfig.HTTP_HEADER)).thenReturn(ID_TOKEN);
        when(jwtConfiguration.getCognitoIdentityPoolUrl()).thenReturn("random_issuer");
        when(configurableJWTProcessor.process(anyString(), any())).thenReturn(claimsSet);
        try {
            processor.getSessionAuthentication(request);
        } catch (CognitoException e){
            assertInvalidIssuerException(e);
        }
    }

    @Test
    public void getSessionAuthenticationWithInvalidAccessTokenThrowsCognitoException() throws Exception {
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .issuer(POOL_URL)
                .claim(AWSConfig.USERNAME, USERNAME)
                .claim(AWSConfig.GROUP_FIELD, Collections.singletonList("ADMIN"))
                .claim("token_use", "random_id")
                .build();
        when(request.getHeader(AWSConfig.HTTP_HEADER)).thenReturn(ID_TOKEN);
        when(jwtConfiguration.getCognitoIdentityPoolUrl()).thenReturn(POOL_URL);
        when(configurableJWTProcessor.process(anyString(), any())).thenReturn(claimsSet);
        try {
            processor.getSessionAuthentication(request);
        } catch (CognitoException e){
            assertNotTokenException(e, "Access");
        }
    }


    private void assertInvalidIssuerException(CognitoException e) {
        assertEquals(CognitoException.INVALID_TOKEN_EXCEPTION_CODE, e.getErrorCode());
        assertEquals(INVALID_TOKEN, e.getMessage());
        assertTrue(e.getDetailErrorMessage().contains("JWT token doesn't match cognito idp random_issuer"));
    }

    private void assertNotTokenException(CognitoException e, String tokenType) {
        assertEquals(CognitoException.NOT_A_TOKEN_EXCEPTION, e.getErrorCode());
        assertEquals(INVALID_TOKEN, e.getMessage());
        assertEquals(String.format("JWT Token doesn't seem to be an %s Token", tokenType), e.getDetailErrorMessage());
    }

    private void asssertTokenNotFoundException(CognitoException e) {
        assertEquals(CognitoException.NO_TOKEN_PROVIDED_EXCEPTION, e.getErrorCode());
        assertEquals("Invalid Action, no token found", e.getMessage());
        assertEquals("No token found in Http Authorization Header", e.getDetailErrorMessage());
    }
}