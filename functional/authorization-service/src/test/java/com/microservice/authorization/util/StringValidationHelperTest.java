package com.microservice.authorization.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StringValidationHelperTest {

    @InjectMocks
    private StringValidationHelper helper;

    @Test
    public void testValidateEmailFormatReturnSuccess() {
        boolean result = helper.validateEmailFormat("email@gmail.com");
        Assert.assertTrue(result);
    }

    @Test
    public void testValidatePhoneNumberReturnSuccess() {
        boolean result = helper.validatePhoneNumber("08112312312");
        Assert.assertTrue(result);
    }

    @Test
    public void testHasOnlyAlphanumericCharactersReturnSuccess() {
        boolean result = helper.hasOnlyAlphanumericCharacters("DUMMY");
        Assert.assertTrue(result);
    }

    @Test
    public void testIsStateValidReturnSuccess() {
        boolean result = helper.isStateValid("AL");
        Assert.assertTrue(result);
    }

    @Test
    public void testHasSqlInjectionCharactersReturnSuccess() {
        boolean result = helper.hasSqlInjectionCharacters("$$$");
        Assert.assertTrue(result);
    }
}
