package com.microservice.authorization.authentication;

import com.microservice.authorization.util.AWSConfig;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.MalformedURLException;

@RunWith(MockitoJUnitRunner.class)
public class CognitoJwtAutoConfigurationTest {
    @Mock
    private AWSConfig jwtConfiguration;

    @InjectMocks
    CognitoJwtAutoConfiguration configuration;

    @Test
    public void testConfigurableJWTProcessorReturnSuccess() throws MalformedURLException {
        String url = "https://cognito-idp.%s.amazonaws.com/%s/.well-known/jwks.json";
        Mockito.when(jwtConfiguration.getJwkUrl()).thenReturn(url);
        ConfigurableJWTProcessor result = configuration.configurableJWTProcessor();
        Assert.assertNotNull(result);
    }
}
