package com.microservice.authorization.validation;

import com.microservice.authorization.model.UpdateUserAttributeRequest;
import com.microservice.authorization.model.UserSignUpRequest;
import com.microservice.authorization.security.model.ErrorMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import static org.junit.Assert.*;

/**
 * @author Ryan_W737 on July 24, 2020
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticationValidatorTest {
    private static final String DUMMY = "DUMMY";
    private static final String BITH_DATE = "01-01-1990";

    @InjectMocks
    private AuthenticationValidator validator;

    private UserSignUpRequest createUserSignUp() {
        UserSignUpRequest request = new UserSignUpRequest();
        request.setAddress(DUMMY);
        request.setEmail("email@gmail.com");
        request.setBirthdate(BITH_DATE);
        request.setPassword("password");
        request.setGender("Male");
        request.setMiddleName(DUMMY);
        request.setName(DUMMY);
        return request;
    }
    @Test
    public void testValidation() {
        UserSignUpRequest request = createUserSignUp();
        List<ErrorMessage> response = validator.validate(request);
        assertEquals(0, response.size());
    }

    @Test
    public void testValidationWithNullAddress() {
        UserSignUpRequest request = createUserSignUp();
        List<ErrorMessage> response = validator.validate(request);
        assertEquals(0, response.size());
    }

    @Test
    public void testValidationWithNullRequest() {
        UserSignUpRequest request = new UserSignUpRequest();
        List<ErrorMessage> response = validator.validate(request);
        assertEquals(7, response.size());
    }

    @Test
    public void testValidationWithInvalidBirthDate() {
        UserSignUpRequest request = createUserSignUp();
        request.setBirthdate("01-Jan-2020");
        List<ErrorMessage> response = validator.validate(request);
        assertEquals("Invalid format birth date", response.get(0).getCode());
    }

    @Test
    public void testValidationWithInvalidBirthDateValue() {
        UserSignUpRequest request = createUserSignUp();
        request.setBirthdate("32-02-1999");
        List<ErrorMessage> response = validator.validate(request);
        assertEquals("Invalid date", response.get(0).getCode());
    }

    @Test
    public void testValidationWithInvalidEmail() {
        UserSignUpRequest request = createUserSignUp();
        request.setEmail("email@email");
        List<ErrorMessage> response = validator.validate(request);
        assertEquals("Invalid format email", response.get(0).getCode());
    }

    @Test
    public void testValidationWithInvalidPassword() {
        UserSignUpRequest request = createUserSignUp();
        request.setPassword("12345");
        List<ErrorMessage> response = validator.validate(request);
        assertEquals("Password must be between 6 and 20 characters", response.get(0).getCode());
    }

    @Test
    public void testValidateUserReturnSuccess() {
        UpdateUserAttributeRequest request = new UpdateUserAttributeRequest();
        request.setBirthdate(BITH_DATE);
        List<ErrorMessage> response = validator.validateUser(request);
        assertEquals(0, response.size());
    }

    @Test
    public void testValidationUpdateUserWithInvalidBirthDateFormat() {
        UpdateUserAttributeRequest request = new UpdateUserAttributeRequest();
        request.setBirthdate("01-Jan-1999");
        List<ErrorMessage> response = validator.validateUser(request);
        assertEquals("Invalid format birth date", response.get(0).getCode());
    }

    @Test
    public void testValidationUpdateUserWithInvalidBirthDateValue() {
        UpdateUserAttributeRequest request = new UpdateUserAttributeRequest();
        request.setBirthdate("32-02-1999");
        List<ErrorMessage> response = validator.validateUser(request);
        assertEquals("Invalid date", response.get(0).getCode());
    }
}
