package com.microservice.authorization.service;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.*;
import com.microservice.authorization.authentication.AWSClientProviderBuilder;
import com.microservice.authorization.exception.CognitoException;
import com.microservice.authorization.model.*;
import com.microservice.authorization.security.model.SpringSecurityUser;
import com.microservice.authorization.util.AWSConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AWSCognitoAuthServiceTests {

    @InjectMocks
    AWSCognitoAuthService cognitoAuthService;

    @Mock
    static AWSClientProviderBuilder cognitoBuilder;

    @Mock
    private static AWSConfig awsConfig;

    @Mock
    private AWSCognitoIdentityProvider awsCognitoIdentityProvider;

    private static final String USER_NAME = "user1@mail.com";
    private static final String EMAIL = "user1@mail.com";
    private static final String PASSWORD = "P@sw0rd!";              //NOSONAR
    private static final String CONFCODE = "587496";
    private static final String ACCESS_TOKEN = "sdwq21123sadsad4565156";
    private static final String ID_TOKEN = "21123sadsad4565156qwewq";
    private static final String NAME = "NameTest";
    private static final String MIDDLE_NAME = "Howard";
    private static final String ADDRESS = "ByPassNgurahRai 20012";
    private static final String DOB = "4-dec-1986";
    private static final String GENDER = "male";
    private static final String SECRET_HASH = "SECRET_HASH";
    private static final String TOKEN = "accessToken";
    private static final String SUCCESS = "SUCCESS";
    private static final String GROUP_NAME = "USER";
    private static final String INVALID_USERNAME = "Invalid Username";

    private void setup() {
        AdminGetUserResult adminGetUserResult = new AdminGetUserResult();
        adminGetUserResult.setEnabled(false);
        adminGetUserResult.setUserLastModifiedDate(new Date());
        adminGetUserResult.setUserCreateDate(new Date());

        when(awsCognitoIdentityProvider.signUp(any(SignUpRequest.class))).thenReturn(new SignUpResult());
        when(awsCognitoIdentityProvider.adminGetUser(any(AdminGetUserRequest.class))).thenReturn(adminGetUserResult);
    }

    private AuthenticationResultType createAuthResult() {
        AuthenticationResultType authenticationResultType = new AuthenticationResultType();
        authenticationResultType.setAccessToken(ACCESS_TOKEN);
        authenticationResultType.setIdToken(ID_TOKEN);
        authenticationResultType.setExpiresIn(1);
        return authenticationResultType;
    }

    @Before
    public void setUp() {
        when(cognitoBuilder.getAWSCognitoIdentityClient()).thenReturn(awsCognitoIdentityProvider);
    }

    @Test
    public void authenticateReturnSUCCESS() throws InvalidKeyException, NoSuchAlgorithmException {
        InitiateAuthResult initiateAuthResult = new InitiateAuthResult();
        initiateAuthResult.setAuthenticationResult(createAuthResult());
        when(cognitoBuilder.calculateSecretHash(anyString())).thenReturn(SECRET_HASH);
        when(awsCognitoIdentityProvider.initiateAuth(ArgumentMatchers.any(InitiateAuthRequest.class)))
                .thenReturn(initiateAuthResult);
        AuthenticationRequest request = new AuthenticationRequest();
        request.setUsername(USER_NAME);
        request.setPassword(PASSWORD);
        SpringSecurityUser result = cognitoAuthService.authenticate(request);

        assertEquals(ACCESS_TOKEN, result.getAccessToken());
        assertEquals(USER_NAME, result.getUsername());
    }

    @Test(expected = CognitoException.class)
    public void authenticateThrowsAwsCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.initiateAuth(ArgumentMatchers.any(InitiateAuthRequest.class)))
                .thenThrow(AWSCognitoIdentityProviderException.class);
        cognitoAuthService.authenticate(new AuthenticationRequest());
    }

    @Test(expected = CognitoException.class)
    public void authenticateThrowsCognitoException() {
        when(awsCognitoIdentityProvider.initiateAuth(ArgumentMatchers.any(InitiateAuthRequest.class)))
                .thenThrow(CognitoException.class);
        cognitoAuthService.authenticate(new AuthenticationRequest());
    }

    @Test
    public void authenticateThrowsException() {
        try {
            cognitoAuthService.authenticate(new AuthenticationRequest());
        } catch (CognitoException e) {
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test
    public void signUpReturnSUCCESS() throws InvalidKeyException, NoSuchAlgorithmException {
        setup();
        when(cognitoBuilder.calculateSecretHash(anyString())).thenReturn(SECRET_HASH);
        when(awsConfig.getDeveloperGroup()).thenReturn(GROUP_NAME);
        UserSignUpRequest request = new UserSignUpRequest();
        request.setEmail(USER_NAME);
        request.setPassword(PASSWORD);
        UserSignUpResponse result = cognitoAuthService.signUp(request);

        assertEquals(USER_NAME, result.getEmail());
    }

    @Test
    public void signUpReturnAddingToOrganizerGroupSUCCESS() throws InvalidKeyException, NoSuchAlgorithmException {
        setup();
        when(cognitoBuilder.calculateSecretHash(anyString())).thenReturn(SECRET_HASH);
        UserSignUpRequest request = new UserSignUpRequest();
        request.setEmail(USER_NAME);
        request.setPassword(PASSWORD);
        request.setOrganizer(true);
        UserSignUpResponse result = cognitoAuthService.signUp(request);

        assertEquals(USER_NAME, result.getEmail());
        assertEquals("ORGANIZER", result.getUserRole());
    }

    @Test(expected = CognitoException.class)
    public void signUpThrowsAwsCognitoIdentityProviderException() {
        setup();
        when(awsCognitoIdentityProvider.signUp(any())).thenThrow(AWSCognitoIdentityProviderException.class);
        cognitoAuthService.signUp(new UserSignUpRequest());
    }

    @Test(expected = CognitoException.class)
    public void signUpThrowsCognitoException() {
        setup();
        when(awsCognitoIdentityProvider.signUp(any())).thenThrow(CognitoException.class);
        cognitoAuthService.signUp(new UserSignUpRequest());
    }

    @Test
    public void signUpThrowsException() {
        try {
            cognitoAuthService.signUp(new UserSignUpRequest());
        } catch (CognitoException e) {
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test
    public void signUpConfirmationReturnSUCCESS() throws InvalidKeyException, NoSuchAlgorithmException {
        setup();
        InitiateAuthResult initiateAuthResult = new InitiateAuthResult();
        initiateAuthResult.setAuthenticationResult(createAuthResult());

        GetUserResult getUserResult = new GetUserResult();
        getUserResult.setUserAttributes(new ArrayList<>());

        final AdminListGroupsForUserResult groupsForUserResult = new AdminListGroupsForUserResult();
        GroupType groupType = new GroupType();
        groupType.setGroupName(GROUP_NAME);
        groupsForUserResult.withGroups(groupType);

        when(awsCognitoIdentityProvider.confirmSignUp(any(ConfirmSignUpRequest.class)))
                .thenReturn(new ConfirmSignUpResult());
        when(cognitoBuilder.calculateSecretHash(anyString())).thenReturn(SECRET_HASH);
        when(awsCognitoIdentityProvider.initiateAuth(any(InitiateAuthRequest.class))).thenReturn(initiateAuthResult);
        when(awsCognitoIdentityProvider.getUser(any(GetUserRequest.class))).thenReturn(getUserResult);
        when(awsCognitoIdentityProvider.adminListGroupsForUser(any(AdminListGroupsForUserRequest.class)))
                .thenReturn(groupsForUserResult);

        UserSignUpConfirmationRequest request = new UserSignUpConfirmationRequest();
        request.setEmail(USER_NAME);
        request.setConfirmationCode(CONFCODE);

        UserSignUpResponse result = cognitoAuthService.signUpConfirmation(request);

        assertEquals(USER_NAME, result.getEmail());
        assertEquals(ACCESS_TOKEN, result.getAccessToken());
        assertEquals(ID_TOKEN, result.getIdToken());
    }

    @Test(expected = CognitoException.class)
    public void signUpConfirmationThrowsAwsCognitoIdentityProviderException() {
        setup();
        when(awsCognitoIdentityProvider.confirmSignUp(any()))
                .thenThrow(AWSCognitoIdentityProviderException.class);

        UserSignUpConfirmationRequest request = new UserSignUpConfirmationRequest();
        request.setEmail(USER_NAME);
        request.setConfirmationCode(CONFCODE);
        cognitoAuthService.signUpConfirmation(request);
    }

    @Test(expected = CognitoException.class)
    public void signUpConfirmationThrowsException() {
        setup();
        UserSignUpConfirmationRequest request = new UserSignUpConfirmationRequest();
        request.setEmail(USER_NAME);
        cognitoAuthService.signUpConfirmation(request);
    }

    @Test
    public void signUpConfirmationThrowsCognitoException() {
        try{
            setup();
            UserSignUpConfirmationRequest request = new UserSignUpConfirmationRequest();
            request.setConfirmationCode(CONFCODE);
            cognitoAuthService.signUpConfirmation(request);
        } catch (CognitoException e){
            assertEquals("Invalid email", e.getMessage());
            assertEquals(CognitoException.EMAIL_MISSING_EXCEPTION, e.getErrorCode());
        }
    }

    @Test
    public void signInReturnSUCCESS() throws InvalidKeyException, NoSuchAlgorithmException {
        InitiateAuthResult initiateAuthResult = new InitiateAuthResult();
        initiateAuthResult.setAuthenticationResult(createAuthResult());

        GetUserResult getUserResult = new GetUserResult();
        getUserResult.setUsername(USER_NAME);
        getUserResult.setUserAttributes(new ArrayList<>());

        final AdminListGroupsForUserResult groupsForUserResult = new AdminListGroupsForUserResult();
        GroupType groupType = new GroupType();
        groupType.setGroupName(GROUP_NAME);
        groupsForUserResult.withGroups(groupType);

        when(cognitoBuilder.calculateSecretHash(anyString())).thenReturn(SECRET_HASH);
        when(awsCognitoIdentityProvider.initiateAuth(any(InitiateAuthRequest.class))).thenReturn(initiateAuthResult);
        when(awsCognitoIdentityProvider.getUser(any(GetUserRequest.class))).thenReturn(getUserResult);
        when(awsCognitoIdentityProvider.adminListGroupsForUser(any(AdminListGroupsForUserRequest.class)))
                .thenReturn(groupsForUserResult);

        AuthenticationRequest request = new AuthenticationRequest();
        request.setUsername(USER_NAME);
        request.setPassword(PASSWORD);
        AuthenticationResponse result = cognitoAuthService.signIn(request);

        assertEquals(ACCESS_TOKEN, result.getAccessToken());
        UserResponse userResponse = result.getUserData();
        assertEquals(USER_NAME, userResponse.getUsername());
    }

    @Test
    public void signInThrowsAwsCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.initiateAuth(any(InitiateAuthRequest.class)))
                .thenThrow(new AWSCognitoIdentityProviderException("NotAuthorizedException & Incorrect username or password."));

        try {
            cognitoAuthService.signIn(new AuthenticationRequest());
        } catch (CognitoException e) {
            // check convertErrorMessageFromExceptionMessage method
            assertEquals("Incorrect username or password.", e.getErrorMessage());
        }

    }

    @Test(expected = CognitoException.class)
    public void signInThrowsCognitoException() {
        when(awsCognitoIdentityProvider.initiateAuth(any(InitiateAuthRequest.class)))
                .thenThrow(CognitoException.class);

        cognitoAuthService.signIn(new AuthenticationRequest());
    }

    @Test
    public void signInThrowsException() {
        try {
            AuthenticationRequest request = new AuthenticationRequest();
            cognitoAuthService.signIn(request);
        } catch (CognitoException e) {
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test
    public void signOutReturnNullAccessTokenThrowCognitoException() {
        try {
            cognitoAuthService.signOut(null, null);
        }
        catch (CognitoException ex){
            assertEquals("Missing access token", ex.getMessage());
            assertEquals(CognitoException.ACCESS_TOKEN_MISSING_EXCEPTION, ex.getErrorCode());
        }
    }

    @Test
    public void signOutReturnSUCCESS() {
        String result = cognitoAuthService.signOut(TOKEN, null);
        assertEquals(SUCCESS, result);
    }

    @Test(expected = CognitoException.class)
    public void signOutReturnThrowAWSCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.globalSignOut(any())).thenThrow(AWSCognitoIdentityProviderException.class);
        cognitoAuthService.signOut(TOKEN, USER_NAME);
    }

    @Test
    public void signOutReturnThrowException() {
        when(awsCognitoIdentityProvider.globalSignOut(any())).thenThrow(NullPointerException.class);
        try {
            cognitoAuthService.signOut(TOKEN, USER_NAME);
        } catch (CognitoException e) {
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test
    public void changePasswordReturnSUCCESS() throws InvalidKeyException, NoSuchAlgorithmException {
        InitiateAuthResult initiateAuthResult = new InitiateAuthResult();
        initiateAuthResult.setAuthenticationResult(createAuthResult());

        GetUserResult getUserResult = new GetUserResult();
        getUserResult.setUsername(USER_NAME);
        getUserResult.setUserAttributes(new ArrayList<>());

        final AdminListGroupsForUserResult groupsForUserResult = new AdminListGroupsForUserResult();
        GroupType groupType = new GroupType();
        groupType.setGroupName(GROUP_NAME);
        groupsForUserResult.withGroups(groupType);

        when(cognitoBuilder.calculateSecretHash(anyString())).thenReturn(SECRET_HASH);
        when(awsCognitoIdentityProvider.initiateAuth(any(InitiateAuthRequest.class))).thenReturn(initiateAuthResult);
        when(awsCognitoIdentityProvider.getUser(any(GetUserRequest.class))).thenReturn(getUserResult);
        when(awsCognitoIdentityProvider.changePassword(any(ChangePasswordRequest.class)))
                .thenReturn(new ChangePasswordResult());
        when(awsCognitoIdentityProvider.adminListGroupsForUser(any(AdminListGroupsForUserRequest.class)))
                .thenReturn(groupsForUserResult);

        PasswordRequest request = new PasswordRequest();
        request.setUsername(USER_NAME);
        request.setNewPassword(PASSWORD);
        request.setOldPassword(PASSWORD);

        PasswordResponse result = cognitoAuthService.changePassword(request);

        assertEquals(USER_NAME, result.getUsername());
        assertEquals(SUCCESS, result.getMessage());
    }

    @Test
    public void changePasswordWithoutUsernameThrowCognitoException() {
        try {
            PasswordRequest request = new PasswordRequest();
            cognitoAuthService.changePassword(request);
        } catch (CognitoException e) {
            assertEquals(INVALID_USERNAME, e.getMessage());
            assertEquals(CognitoException.INVALID_USERNAME_EXCEPTION, e.getErrorCode());
        }
    }

    @Test
    public void changePasswordThrowCognitoException() {
        try {
            PasswordRequest request = new PasswordRequest();
            request.setUsername(USER_NAME);
            cognitoAuthService.changePassword(request);
        } catch (CognitoException e) {
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test
    public void forgotPasswordReturnSUCCESS() throws InvalidKeyException, NoSuchAlgorithmException {
        ListUsersResult usersResult = new ListUsersResult();
        List<UserType> userTypes = new ArrayList<>();
        UserType userType = new UserType();
        userType.setUsername(USER_NAME);
        userTypes.add(userType);
        usersResult.setUsers(userTypes);

        ForgotPasswordResult passwordResult = new ForgotPasswordResult();
        passwordResult.setCodeDeliveryDetails(new CodeDeliveryDetailsType());

        when(cognitoBuilder.calculateSecretHash(anyString())).thenReturn(SECRET_HASH);
        when(awsCognitoIdentityProvider.listUsers(any(ListUsersRequest.class))).thenReturn(usersResult);
        when(awsCognitoIdentityProvider.forgotPassword(any(ForgotPasswordRequest.class)))
                .thenReturn(passwordResult);

        PasswordRequest request = new PasswordRequest();
        request.setUsername(USER_NAME);

        PasswordResponse result = cognitoAuthService.forgotPassword(request);

        assertEquals(USER_NAME, result.getUsername());
        assertEquals(SUCCESS, result.getMessage());
    }

    @Test(expected = CognitoException.class)
    public void forgotPasswordThrowsAWSCognitoIdentityProviderException() {
        ListUsersResult usersResult = new ListUsersResult();
        List<UserType> userTypes = new ArrayList<>();
        UserType userType = new UserType();
        userType.setUsername(USER_NAME);
        userTypes.add(userType);
        usersResult.setUsers(userTypes);

        when(awsCognitoIdentityProvider.listUsers(any(ListUsersRequest.class))).thenReturn(usersResult);
        when(awsCognitoIdentityProvider.forgotPassword(any(ForgotPasswordRequest.class)))
                .thenThrow(AWSCognitoIdentityProviderException.class);

        PasswordRequest request = new PasswordRequest();
        request.setUsername(USER_NAME);
        cognitoAuthService.forgotPassword(request);
    }

    @Test
    public void forgotPasswordWithEmptyUsernameThrowsCognitoException() {
        try {
            PasswordRequest request = new PasswordRequest();
            cognitoAuthService.forgotPassword(request);
        } catch (CognitoException e) {
            assertEquals(INVALID_USERNAME, e.getMessage());
            assertEquals(CognitoException.INVALID_USERNAME_EXCEPTION, e.getErrorCode());
        }
    }

    @Test
    public void confirmForgotPasswordReturnSUCCESS() {
        PasswordRequest request = new PasswordRequest();
        request.setUsername(USER_NAME);
        request.setConfirmationCode(CONFCODE);
        request.setNewPassword(PASSWORD);

        PasswordResponse result = cognitoAuthService.confirmForgotPassword(request);

        assertEquals(USER_NAME, result.getUsername());
        assertEquals(SUCCESS, result.getMessage());
    }

    @Test(expected = CognitoException.class)
    public void confirmForgotPasswordThrowsAWSCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.confirmForgotPassword(any(ConfirmForgotPasswordRequest.class)))
                .thenThrow(AWSCognitoIdentityProviderException.class);
        PasswordRequest request = new PasswordRequest();
        request.setUsername(USER_NAME);
        cognitoAuthService.confirmForgotPassword(request);
    }

    @Test
    public void confirmForgotPasswordWithoutUsernameThrowCognitoException() {
        PasswordRequest request = new PasswordRequest();
        request.setConfirmationCode(CONFCODE);
        request.setNewPassword(PASSWORD);
        try {
            cognitoAuthService.confirmForgotPassword(request);
        } catch (CognitoException e) {
            assertEquals(INVALID_USERNAME, e.getMessage());
        }
    }

    @Test
    public void confirmForgotPasswordThrowException() {
        when(awsCognitoIdentityProvider.confirmForgotPassword(any(ConfirmForgotPasswordRequest.class)))
                .thenThrow(NullPointerException.class);
        PasswordRequest request = new PasswordRequest();
        request.setUsername(USER_NAME);
        try {
            cognitoAuthService.confirmForgotPassword(request);
        } catch (CognitoException e) {
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test
    public void resendConfirmationCodeReturnSUCCESS() throws InvalidKeyException, NoSuchAlgorithmException {
        ResendConfirmationCodeResult confirmationCodeResult = new ResendConfirmationCodeResult();
        confirmationCodeResult.setCodeDeliveryDetails(new CodeDeliveryDetailsType());

        when(cognitoBuilder.calculateSecretHash(anyString())).thenReturn(SECRET_HASH);
        when(awsCognitoIdentityProvider.resendConfirmationCode(any(ResendConfirmationCodeRequest.class)))
                .thenReturn(confirmationCodeResult);

        ResendConfCodeRequest request = new ResendConfCodeRequest();
        request.setUsername(USER_NAME);

        UserSignUpResponse result = cognitoAuthService.resendConfirmationCode(request);
        assertEquals(USER_NAME, result.getEmail());
    }

    @Test(expected = CognitoException.class)
    public void resendConfirmationCodeThrowsAWSCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.resendConfirmationCode(any(ResendConfirmationCodeRequest.class)))
                .thenThrow(AWSCognitoIdentityProviderException.class);
        ResendConfCodeRequest request = new ResendConfCodeRequest();
        request.setUsername(USER_NAME);
        cognitoAuthService.resendConfirmationCode(request);
    }

    @Test(expected = CognitoException.class)
    public void resendConfirmationCodeThrowsException() {
        ResendConfCodeRequest request = new ResendConfCodeRequest();
        request.setUsername(USER_NAME);
        cognitoAuthService.resendConfirmationCode(request);
    }

    @Test
    public void resendConfirmationCodeWithoutUsernameThrowsCognitoException() {
        try {
            ResendConfCodeRequest request = new ResendConfCodeRequest();
            cognitoAuthService.resendConfirmationCode(request);
        } catch (CognitoException e) {
            assertEquals(INVALID_USERNAME, e.getMessage());
            assertEquals(CognitoException.EMAIL_MISSING_EXCEPTION, e.getErrorCode());

        }
    }

    @Test(expected = CognitoException.class)
    public void addUserToGroupThrowsAWSCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.adminAddUserToGroup(any(AdminAddUserToGroupRequest.class)))
                .thenThrow(AWSCognitoIdentityProviderException.class);
        cognitoAuthService.addUserToGroup(USER_NAME, GROUP_NAME);
    }

    @Test
    public void addUserToGroupThrowException() {
        when(awsCognitoIdentityProvider.adminAddUserToGroup(any(AdminAddUserToGroupRequest.class)))
                .thenThrow(NullPointerException.class);
        try {
            cognitoAuthService.addUserToGroup(USER_NAME, GROUP_NAME);
        } catch (CognitoException e){
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test(expected = CognitoException.class)
    public void removeUserFromGroupThrowsAWSCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.adminRemoveUserFromGroup(any(AdminRemoveUserFromGroupRequest.class)))
                .thenThrow(AWSCognitoIdentityProviderException.class);
        cognitoAuthService.removeUserFromGroup(USER_NAME, GROUP_NAME);
    }

    @Test
    public void removeUserFromGroupThrowException() {
        when(awsCognitoIdentityProvider.adminRemoveUserFromGroup(any(AdminRemoveUserFromGroupRequest.class)))
                .thenThrow(NullPointerException.class);
        try {
            cognitoAuthService.removeUserFromGroup(USER_NAME, GROUP_NAME);
        } catch (CognitoException e){
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test(expected = CognitoException.class)
    public void GetUserRoleThrowsAWSCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.adminListGroupsForUser(any(AdminListGroupsForUserRequest.class)))
                .thenThrow(AWSCognitoIdentityProviderException.class);
        cognitoAuthService.getUserRole(USER_NAME);
    }

    @Test
    public void GetUserRoleThrowException() {
        when(awsCognitoIdentityProvider.adminListGroupsForUser(any(AdminListGroupsForUserRequest.class)))
                .thenThrow(NullPointerException.class);
        try {
            cognitoAuthService.getUserRole(USER_NAME);
        } catch (CognitoException e){
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test
    public void updateUserAttributesReturnSUCCESS() {
        UpdateUserAttributesResult userAttributesResult = new UpdateUserAttributesResult();

        when(awsCognitoIdentityProvider.updateUserAttributes(any(UpdateUserAttributesRequest.class)))
                .thenReturn(userAttributesResult);

        UpdateUserAttributeRequest request = new UpdateUserAttributeRequest();
        request.setAccessToken(ACCESS_TOKEN);
        request.setUsername(USER_NAME);
        request.setAddress(ADDRESS);
        request.setBirthdate(DOB);
        request.setGender(GENDER);
        request.setMiddleName(MIDDLE_NAME);
        request.setName(NAME);

        UpdateUserAttributeResponse result = cognitoAuthService.updateUserAttributes(request);

        assertEquals(USER_NAME, result.getUsername());
        assertEquals(SUCCESS, result.getMessage());
    }

    @Test(expected = CognitoException.class)
    public void updateUserAttributesThrowsAWSCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.updateUserAttributes(any(UpdateUserAttributesRequest.class)))
                .thenThrow(AWSCognitoIdentityProviderException.class);

        cognitoAuthService.updateUserAttributes(new UpdateUserAttributeRequest());
    }

    @Test(expected = CognitoException.class)
    public void updateUserAttributesThrowsCognitoException() {
        when(awsCognitoIdentityProvider.updateUserAttributes(any(UpdateUserAttributesRequest.class)))
                .thenThrow(CognitoException.class);

        cognitoAuthService.updateUserAttributes(new UpdateUserAttributeRequest());
    }

    @Test
    public void updateUserAttributesThrowsException() {
        when(awsCognitoIdentityProvider.updateUserAttributes(any(UpdateUserAttributesRequest.class)))
                .thenThrow(NullPointerException.class);
        try {
            cognitoAuthService.updateUserAttributes(new UpdateUserAttributeRequest());
        } catch (CognitoException e) {
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test
    public void getUserInfoReturnSUCCESS() {
        GetUserResult getUserResult = new GetUserResult();
        final AttributeType emailAttribute = new AttributeType();
        emailAttribute.setName("email");
        emailAttribute.setValue(EMAIL);
        final AttributeType addressAttribute = new AttributeType();
        addressAttribute.setName("address");
        addressAttribute.setValue(ADDRESS);
        final AttributeType nameAttribute = new AttributeType();
        nameAttribute.setName("name");
        nameAttribute.setValue(NAME);
        final AttributeType middleNameAttribute = new AttributeType();
        middleNameAttribute.setName("middle_name");
        middleNameAttribute.setValue(MIDDLE_NAME);
        final AttributeType genderAttribute = new AttributeType();
        genderAttribute.setName("gender");
        genderAttribute.setValue(GENDER);
        final AttributeType birthDateAttribute = new AttributeType();
        birthDateAttribute.setName("birthdate");
        birthDateAttribute.setValue(DOB);

        final List<AttributeType> listAttributeType = new ArrayList<>
                (Arrays.asList(emailAttribute, addressAttribute, nameAttribute, middleNameAttribute, genderAttribute, birthDateAttribute));

        getUserResult.setUserAttributes(listAttributeType);
        when(awsCognitoIdentityProvider.getUser(any(GetUserRequest.class)))
                .thenReturn(getUserResult);
        final UserResponse result = cognitoAuthService.getUserInfo(ACCESS_TOKEN);

        assertEquals(result.getEmail(), EMAIL);
        assertEquals(result.getAddress(), ADDRESS);
        assertEquals(result.getName(), NAME);
        assertEquals(result.getMiddleName(), MIDDLE_NAME);
        assertEquals(result.getGender(), GENDER);
        assertEquals(result.getBirthDate(), DOB);
    }

    @Test(expected = CognitoException.class)
    public void getUserInfoThrowsAWSCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.getUser(any(GetUserRequest.class)))
                .thenThrow(AWSCognitoIdentityProviderException.class);

        cognitoAuthService.getUserInfo(ACCESS_TOKEN);
    }

    @Test
    public void getUserInfoThrowsException() {
        try {
            cognitoAuthService.getUserInfo(ACCESS_TOKEN);
        } catch (CognitoException e) {
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test
    public void getUserInfoWithBlankTokenThrowsCognitoException() {
        try {
            cognitoAuthService.getUserInfo(null);
        } catch (CognitoException e) {
            assertEquals(CognitoException.INVALID_ACCESS_TOKEN_EXCEPTION, e.getErrorCode());
            assertEquals("User must provide an access token", e.getMessage());
        }
    }

    @Test
    public void deleteUserReturnSUCCESS() {
        when(awsCognitoIdentityProvider.adminDeleteUser(any(AdminDeleteUserRequest.class)))
                .thenReturn(new AdminDeleteUserResult());

        cognitoAuthService.deleteUser(USER_NAME);
    }

    @Test(expected = CognitoException.class)
    public void deleteUserThrowAWSCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.adminDeleteUser(any(AdminDeleteUserRequest.class)))
                .thenThrow(AWSCognitoIdentityProviderException.class);

        cognitoAuthService.deleteUser(USER_NAME);
    }

    @Test
    public void deleteUserThrowException() {
        when(awsCognitoIdentityProvider.adminDeleteUser(any(AdminDeleteUserRequest.class))).thenThrow(NullPointerException.class);
        try {
            cognitoAuthService.deleteUser(USER_NAME);
        } catch (CognitoException e) {
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }

    @Test(expected = CognitoException.class)
    public void filterListUsersByUsernameThrowsAWSCognitoIdentityProviderException() {
        when(awsCognitoIdentityProvider.listUsers(any(ListUsersRequest.class))).thenThrow(AWSCognitoIdentityProviderException.class);
        cognitoAuthService.filterListUsersByUsername(USER_NAME);
    }

    @Test
    public void filterListUsersByUsernameThrowsException() {
        try {
            cognitoAuthService.filterListUsersByUsername(USER_NAME);
        } catch (CognitoException e) {
            assertEquals(CognitoException.GENERIC_EXCEPTION_CODE, e.getErrorCode());
        }
    }
}