package com.microservice.authorization.security.filter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SecurityAuthenticationEntryPointTest {
    private static final String USER_NOT_FOUND = "Username not found";

    @InjectMocks
    private SecurityAuthenticationEntryPoint entryPoint;

    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private HttpServletResponse servletResponse;

    @Test
    public void testCommenceReturnSuccess() throws IOException {
        HttpServletResponse response = new HttpServletResponseWrapper(servletResponse);
        PrintWriter writer = new PrintWriter(USER_NOT_FOUND);
        when(response.getWriter()).thenReturn(writer);
        AuthenticationException exception = new UsernameNotFoundException(USER_NOT_FOUND);
        entryPoint.commence(servletRequest, servletResponse, exception);
    }
}