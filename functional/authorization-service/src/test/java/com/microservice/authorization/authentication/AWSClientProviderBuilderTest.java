package com.microservice.authorization.authentication;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.microservice.authorization.util.AWSConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AWSClientProviderBuilderTest {
    private static final String DUMMY_STRING = "DUMMY";

    @Mock
    private AWSConfig cognitoConfig;

    @InjectMocks
    AWSClientProviderBuilder builder;

    @Test
    public void testGetAWSCognitoIdentityWithValidAWSProviderClientReturnSuccess() {
        AWSCognitoIdentityProvider cognito = mock(AWSCognitoIdentityProvider.class);
        when(cognitoConfig.getRegion()).thenReturn(DUMMY_STRING);
        AWSCognitoIdentityProvider result = builder.getAWSCognitoIdentityClient();
        assertNotEquals(cognito, result);
    }

    @Test
    public void testGetAWSS3ClientWithValidAWSProviderClientReturnSuccess() {
        AmazonS3 cognito = mock(AmazonS3.class);
        when(cognitoConfig.getRegion()).thenReturn(DUMMY_STRING);
        AmazonS3 result = builder.getAWSS3Client();
        assertNotEquals(cognito, result);
    }

    @Test
    public void testCalculateSecretHashReturnSuccess() throws InvalidKeyException, NoSuchAlgorithmException {
        when(cognitoConfig.getClientSecret()).thenReturn(DUMMY_STRING);
        when(cognitoConfig.getClientId()).thenReturn(DUMMY_STRING);
        String result = builder.calculateSecretHash(DUMMY_STRING);
        assertEquals("2Vwlu8S26ESuexdBBLiV46YTlxGAn6FRCdpVWMKG83k=", result);
    }
}
