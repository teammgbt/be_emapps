package com.microservice.authorization.security.filter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.access.AccessDeniedException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RestAccessDeniedHandlerTest {
    private static final String ACCESS_DENIED = "Access Denied";

    @InjectMocks
    private RestAccessDeniedHandler restAccessDeniedHandler;

    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private HttpServletResponse servletResponse;

    @Test
    public void testHandleReturnSuccess() throws IOException {
        HttpServletResponse response = new HttpServletResponseWrapper(servletResponse);
        PrintWriter writer = new PrintWriter(ACCESS_DENIED);
        when(response.getWriter()).thenReturn(writer);
        AccessDeniedException accessDeniedException = new AccessDeniedException(ACCESS_DENIED);
        restAccessDeniedHandler.handle(servletRequest, servletResponse, accessDeniedException);
    }
}