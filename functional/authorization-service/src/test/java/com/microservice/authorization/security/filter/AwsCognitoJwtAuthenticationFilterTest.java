package com.microservice.authorization.security.filter;

import com.microservice.authorization.controller.ExceptionController;
import com.microservice.authorization.exception.CognitoException;
import com.microservice.authorization.security.config.CognitoJwtAuthentication;
import com.microservice.authorization.security.model.ResponseWrapper;
import com.nimbusds.jose.proc.BadJOSEException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonMap;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AwsCognitoJwtAuthenticationFilterTest {

    @InjectMocks
    private AwsCognitoJwtAuthenticationFilter authFilter;

    @Mock
    private AwsCognitoTokenProcessor awsCognitoTokenProcessor;

    @Mock
    private HttpServletResponse response;

    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private FilterChain filterChain;

    @Mock
    private ApplicationContext appContext;

    @Mock
    private ExceptionController exceptionController;

    private static final String USER_NAME = "emapps.mitrais@gmail.com";
    private static final String ERROR_MESSAGE = "This is test error Message";
    private static final String ERROR_CODE = "ERROR_MESSAGE";
    private static final String ERROR_DETAIL = "This is test error Message with Detail (00110011)";
    private static final String STATUS = "status";
    private static final String USER_NOT_FOUND = "Username not found";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDoFilterInternalReturnSuccess() throws Exception {

        HttpServletRequest request = new HttpServletRequestWrapper(servletRequest);
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        User user = new User(USER_NAME, "", grantedAuthorities);
        Authentication authentication = new CognitoJwtAuthentication(user, null, new ArrayList<>());
        when(awsCognitoTokenProcessor.getIdAuthentication(request)).thenReturn(authentication);
        authFilter.doFilter(request, response, filterChain);
    }

    @Test
    public void testDoFilterReturnBadJOSEException() throws Exception {
        HttpServletRequest request = new HttpServletRequestWrapper(servletRequest);
        ResponseWrapper wrapper = new ResponseWrapper(null,
                singletonMap(STATUS, HttpStatus.NOT_ACCEPTABLE), null);

        when(awsCognitoTokenProcessor.getIdAuthentication(request)).thenThrow(BadJOSEException.class);
        when(appContext.getBean(ExceptionController.class)).thenReturn(exceptionController);
        when(exceptionController.handleJwtException(any(CognitoException.class))).thenReturn(wrapper);
        authFilter.doFilterInternal(request, response, filterChain);
    }

    @Test
    public void testDoFilterReturnCognitoException() throws Exception {
        HttpServletRequest request = new HttpServletRequestWrapper(servletRequest);
        ResponseWrapper wrapper = new ResponseWrapper(null,
                singletonMap(STATUS, HttpStatus.NOT_ACCEPTABLE), null);

        when(awsCognitoTokenProcessor.getIdAuthentication(request)).thenThrow(CognitoException.class);
        when(appContext.getBean(ExceptionController.class)).thenReturn(exceptionController);
        when(exceptionController.handleJwtException(any(CognitoException.class))).thenReturn(wrapper);
        authFilter.doFilterInternal(request, response, filterChain);
    }

    @Test
    public void testDoFilterReturnException() throws Exception {
        HttpServletRequest request = new HttpServletRequestWrapper(servletRequest);
        ResponseWrapper wrapper = new ResponseWrapper(null,
                singletonMap(STATUS, HttpStatus.NOT_ACCEPTABLE), null);

        when(awsCognitoTokenProcessor.getIdAuthentication(request)).thenThrow(NullPointerException.class);
        when(appContext.getBean(ExceptionController.class)).thenReturn(exceptionController);
        when(exceptionController.handleJwtException(any(CognitoException.class))).thenReturn(wrapper);
        authFilter.doFilterInternal(request, response, filterChain);
    }
}