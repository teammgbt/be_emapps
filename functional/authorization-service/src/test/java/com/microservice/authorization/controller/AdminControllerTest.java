package com.microservice.authorization.controller;

import com.microservice.authorization.model.AuthenticationResponse;
import com.microservice.authorization.model.UserRequest;
import com.microservice.authorization.security.model.ResponseWrapper;
import com.microservice.authorization.service.AWSCognitoAuthService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Objects;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AdminControllerTest {

    @InjectMocks
    AdminController adminController;

    @Mock
    AWSCognitoAuthService authService;

    private static final String USER_NAME = "userName2@mail.com";
    private static final String USER_ROLE = "USER";
    private static final String ID_TOKEN = "Bearer idtoken123";

    @Test
    public void addUserToGroupReturnSuccessMessage() {
        final UserRequest userRequest = new UserRequest();
        userRequest.setUsername(USER_NAME);
        userRequest.setUserRole(USER_ROLE);
        userRequest.setAccessToken(ID_TOKEN);
        ResponseEntity<ResponseWrapper> responseEntity = adminController.addUserToGroup(userRequest);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());
        assertNull(responseWrapper.getErrors());

        final AuthenticationResponse authResponse = (AuthenticationResponse) responseWrapper.getData();
        assertEquals(USER_NAME, authResponse.getUsername());
        assertEquals("User userName2@mail.com added to group USER", authResponse.getMessage());
    }

    @Test
    public void removeUserFromGroupReturnSuccessMessage() {
        final UserRequest userRequest = new UserRequest();
        userRequest.setUsername(USER_NAME);
        userRequest.setUserRole(USER_ROLE);
        ResponseEntity<ResponseWrapper> responseEntity = adminController.removeUserFromGroup(userRequest);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());
        assertNull(responseWrapper.getErrors());

        final AuthenticationResponse authResponse = (AuthenticationResponse) responseWrapper.getData();
        assertEquals(USER_NAME, authResponse.getUsername());
        assertEquals("User userName2@mail.com removed from group USER", authResponse.getMessage());
    }

    @Test
    public void getGroupOfUserReturnSuccessMessage() {
        final UserRequest userRequest = new UserRequest();
        userRequest.setUsername(USER_NAME);

        final ArrayList<String> userRole = new ArrayList<>();
        userRole.add("ORGANIZER");
        when(authService.getUserRole(USER_NAME)).thenReturn(userRole);

        ResponseEntity<ResponseWrapper> responseEntity = adminController.getGroupOfUser(userRequest);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNotNull(Objects.requireNonNull(responseWrapper).getData());
        assertNull(responseWrapper.getErrors());

        final AuthenticationResponse authResponse = (AuthenticationResponse) responseWrapper.getData();
        assertEquals(USER_NAME, authResponse.getUsername());
        assertEquals(userRole, authResponse.getUserRole());
    }

}