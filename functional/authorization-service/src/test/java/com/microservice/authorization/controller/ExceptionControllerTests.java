package com.microservice.authorization.controller;

import com.microservice.authorization.exception.CognitoException;
import com.microservice.authorization.security.model.ErrorMessage;
import com.microservice.authorization.security.model.ResponseWrapper;
import com.microservice.authorization.security.model.RestErrorList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonMap;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExceptionControllerTests {

    @InjectMocks
    ExceptionController exceptionController;

    private static final String ERROR_MESSAGE = "This is test error Message";
    private static final String ERROR_CODE = "ERROR_MESSAGE";
    private static final String ERROR_DETAIL = "This is test error Message with Detail (00110011)";
    private static final String STATUS = "status";

    @Test
    public void handleExceptionInternalTest(){

        Exception ex = new Exception(ERROR_MESSAGE, new Throwable(ERROR_DETAIL));

        //Test
        ResponseEntity<Object> responseEntity = exceptionController.handleExceptionInternal(ex);

        //Assert
        assertEquals(HttpStatus.NOT_ACCEPTABLE, responseEntity.getStatusCode());
    }

    @Test
    public void handleMethodArgumentNotValidTest(){
        String messageCode = "406 NOT_ACCEPTABLE";
        //Mock
        MethodArgumentNotValidException methodArgumentNotValidException = mock(MethodArgumentNotValidException.class);
        BindingResult bindingResult = mock(BindingResult.class);
        when(bindingResult.getAllErrors()).thenReturn(Collections.singletonList(new ObjectError(ERROR_CODE, ERROR_MESSAGE)));
        when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);

        //test
        ResponseEntity<Object> responseEntity = exceptionController.handleMethodArgumentNotValid(methodArgumentNotValidException);
        ResponseWrapper responseWrapper = (ResponseWrapper) responseEntity.getBody();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setMessage(ERROR_MESSAGE);
        errorMessage.setCode(messageCode);
        errorMessage.setDetail(ERROR_DETAIL);
        errorMessages.add(errorMessage);
        responseWrapper.setErrors(errorMessages);

        Map<String, HttpStatus> metadata = (Map<String, HttpStatus>) responseWrapper.getMetadata();
        //Assert
        assertEquals(HttpStatus.NOT_ACCEPTABLE, responseEntity.getStatusCode());
        assertEquals(HttpStatus.NOT_ACCEPTABLE, metadata.get(STATUS));
        assertFalse(responseWrapper.getErrors().isEmpty());
        assertEquals(messageCode, responseWrapper.getErrors().get(0).getCode());
        assertEquals(ERROR_MESSAGE, responseWrapper.getErrors().get(0).getMessage());
        assertEquals(ERROR_DETAIL, responseWrapper.getErrors().get(0).getDetail());
    }

    @Test
    public void handleExceptionTest(){
        CognitoException exception = new CognitoException(ERROR_MESSAGE, ERROR_CODE, ERROR_DETAIL);

        RestErrorList errorList = new RestErrorList(HttpStatus.NOT_ACCEPTABLE, new ErrorMessage(
                exception.getErrorMessage(), exception.getErrorCode(), exception.getDetailErrorMessage()));
        ResponseWrapper responseWrapper = new ResponseWrapper(null,
                singletonMap(STATUS, HttpStatus.NOT_ACCEPTABLE), errorList);

        //test
        ResponseEntity<ResponseWrapper> responseEntity = exceptionController.handleException(responseWrapper);

        //Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());

        ResponseWrapper responseBody = responseEntity.getBody();
        assertEquals(responseWrapper, responseBody);
    }

    @Test
    public void handleIOExceptionTest(){

        CognitoException cognitoException = new CognitoException(ERROR_MESSAGE, ERROR_CODE, ERROR_DETAIL);

        //test
        ResponseEntity<ResponseWrapper> responseEntity = exceptionController.handleIOException(cognitoException);

        //Assert
        assertEquals(HttpStatus.NOT_ACCEPTABLE, responseEntity.getStatusCode());

        ResponseWrapper responseWrapper = responseEntity.getBody();
        assertNull(responseWrapper.getData());

        Map<String, HttpStatus> metadata = (Map<String, HttpStatus>) responseWrapper.getMetadata();
        assertEquals(HttpStatus.NOT_ACCEPTABLE, metadata.get(STATUS));

        assertFalse(responseWrapper.getErrors().isEmpty());
        assertEquals(ERROR_CODE, responseWrapper.getErrors().get(0).getCode());
        assertEquals(ERROR_MESSAGE, responseWrapper.getErrors().get(0).getMessage());
        assertEquals(ERROR_DETAIL, responseWrapper.getErrors().get(0).getDetail());
        RestErrorList restErrorList = (RestErrorList) responseWrapper.getErrors();
        assertEquals(HttpStatus.NOT_ACCEPTABLE, restErrorList.getStatus());
    }

    @Test
    public void handleJwtExceptionTest(){

        CognitoException cognitoException = new CognitoException(ERROR_MESSAGE, ERROR_CODE, ERROR_DETAIL);

        //test
        ResponseWrapper responseWrapper = exceptionController.handleJwtException(cognitoException);

        assertNull(responseWrapper.getData());

        Map<String, HttpStatus> metadata = (Map<String, HttpStatus>) responseWrapper.getMetadata();
        assertEquals(HttpStatus.UNAUTHORIZED, metadata.get(STATUS));

        assertFalse(responseWrapper.getErrors().isEmpty());
        assertEquals(ERROR_CODE, responseWrapper.getErrors().get(0).getCode());
        assertEquals(ERROR_MESSAGE, responseWrapper.getErrors().get(0).getMessage());
        assertEquals(ERROR_DETAIL, responseWrapper.getErrors().get(0).getDetail());
        RestErrorList restErrorList = (RestErrorList) responseWrapper.getErrors();
        assertEquals(HttpStatus.UNAUTHORIZED, restErrorList.getStatus());
    }
}
