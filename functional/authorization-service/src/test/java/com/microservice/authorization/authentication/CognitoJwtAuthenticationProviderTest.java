package com.microservice.authorization.authentication;

import com.microservice.authorization.model.AuthenticationRequest;
import com.microservice.authorization.security.model.SpringSecurityUser;
import com.microservice.authorization.service.AWSCognitoAuthService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CognitoJwtAuthenticationProviderTest {
    private static final String EMAIL = "emapps.mitrais@gmail.com";
    private static final String PASSWORD = "password";
    private static final String USERNAME = "username";

    @Mock
    private AWSCognitoAuthService cognitoService;

    @InjectMocks
    private CognitoJwtAuthenticationProvider authenticationProvider;

    @Test
    public void testAuthenticateReturnSuccess() {
        Authentication authentication = Mockito.mock(Authentication.class);
        Map<String, String> map = new HashMap<>();
        map.put("newPassword", "value1");
        map.put(PASSWORD, "value2");
        SpringSecurityUser securityUser = new SpringSecurityUser(USERNAME, PASSWORD, EMAIL, new Date(),
                new ArrayList<>());
        securityUser.setExpiresIn(5);
        when(authentication.getCredentials()).thenReturn(map);
        when(cognitoService.authenticate(any(AuthenticationRequest.class))).thenReturn(securityUser);
        Authentication result = authenticationProvider.authenticate(authentication);
        Assert.assertEquals(USERNAME, result.getPrincipal());
    }

    @Test
    public void testAuthenticateWithNullSecurityUserReturnNull() {
        Authentication authentication = Mockito.mock(Authentication.class);
        Map<String, String> map = new HashMap<>();
        map.put("newPassword", "value1");
        map.put(PASSWORD, "value2");
        when(authentication.getCredentials()).thenReturn(map);
        when(cognitoService.authenticate(any(AuthenticationRequest.class))).thenReturn(null);
        Authentication result = authenticationProvider.authenticate(authentication);
        Assert.assertNull(result);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testAuthenticateWithNullAuthenticationReturnException() {
        authenticationProvider.authenticate(null);
    }

    @Test
    public void testSupportReturnTrue() {
        boolean result = authenticationProvider.supports(UsernamePasswordAuthenticationToken.class);
        Assert.assertTrue(result);
    }

    @Test
    public void testSupportReturnFalse() {
        boolean result = authenticationProvider.supports(UsernameNotFoundException.class);
        Assert.assertFalse(result);
    }
}
